package mastermind.domini;

import java.util.Date;
import mastermind.persistencia.*;

/**
 * 
 * @author Joel Moreno
 */


public class ControladorDomini {
    
    private Partida partidaActual;
    private String idxPartida;
    public Ranking ranking;


    //Post: s'inicialitzen els atributs del controlador restaurant el
    //      ranking desde persistencia i posant la partida actual buida.
    public ControladorDomini(){ //inicialitzacio del controlador
        String facil = Controlador_Persistencia.loadRanking(0);
        String mitjana = Controlador_Persistencia.loadRanking(1);
        String dificil = Controlador_Persistencia.loadRanking(2);
        ranking = new Ranking(facil, mitjana, dificil);
        partidaActual = null;
        idxPartida = null;
    }

    //Post: retorna l'string d'indexs de partides que reb desde persistencia
    public String sendIndexs(){
        return Controlador_Persistencia.getIndexs();
    }

    //Post: Si tots els atributs de la partida son adequats es crea
    //      una nova partida, s'inicialitza el punter de partida actual
    //      i la partida es guarda a persistencia
    public boolean crearPartida(String nom, int dificultat, String mode, int rol) throws Exception{ //crea una partida amb la ronda inicial 
        String idxs = sendIndexs();
        int nind = 0;
        for(int i = 0; i < idxs.length(); ++i){
            if(idxs.charAt(i) == '|') ++nind;
        }
        if(nind == 10) return false;

        if(dificultat < 0 || dificultat > 2) throw new Exception("La dificultat ha d'estar entre 0 i 2");
        if(rol != 0 && rol != 1) throw new Exception("El rol ha de ser 0(codemaker) o 1(codebreaker)");
        if(mode.compareTo("practica") != 0 && mode.compareTo("competitiu") != 0) throw new Exception("El mode ha de ser practica o competitiu");
        partidaActual = new Partida(mode, dificultat, rol, nom);

        if(partidaActual.getRolInicial() == 1){
            partidaActual.novarondacodebreaker();
			String aGuardar = partidaActual.PartidatoString();
			Date now = new Date();
			String index = now.toString().concat("|").concat(nom);
			idxPartida = index;
			Controlador_Persistencia.writeLine(aGuardar, index);
        }
		
        return true;
    }

    //Post: Crea una ronda codemaker 
    public String creaRondaCodemaker(String solucio,String nom) throws Exception{
        int dif = partidaActual.getdificultat();
        if(dif == 0){
            if(solucio.length() != 3) throw new Exception("Llargada no valida");
            for(int i = 0; i < 3; ++i) if(solucio.charAt(i) > '4' || solucio.charAt(i) < '0') throw new Exception("Color no valid");
        }
        if(dif == 1){
            if(solucio.length() != 4) throw new Exception("Llargada no valida");
            for(int i = 0; i < 4; ++i) if(solucio.charAt(i) > '5' || solucio.charAt(i) < '0') throw new Exception("Color no valid");
        }
        if(dif == 2){
            if(solucio.length() != 5) throw new Exception("Llargada no valida");
            for(int i = 0; i < 5; ++i) if(solucio.charAt(i) > '7' || solucio.charAt(i) < '0') throw new Exception("Color no valid");
        }
		String candidat = partidaActual.novarondacodemaker(solucio);
		if(partidaActual.getNrondes()==1){		
			String aGuardar = partidaActual.PartidatoString();
			Date now = new Date();
			String index = now.toString().concat("|").concat(nom);
			idxPartida = index;
			Controlador_Persistencia.writeLine(aGuardar, index);
		}
		else{
			String aGuardar = partidaActual.PartidatoString();
			Controlador_Persistencia.modifyLine(aGuardar, idxPartida);
		}
        return candidat;
    }

    //Post: realitza una jugada sigui codemaker o codebreaker
    public String jugar(String input) throws Exception{
        String res;
        if(partidaActual.getRolActual() == 0) res = jugarCodemaker(input);
        else res = jugarCodebreaker(input);

        String aGuardar = partidaActual.PartidatoString();
        Controlador_Persistencia.modifyLine(aGuardar, idxPartida);

        return res;
    }

    //Post: Realitza una jugada Codebreaker
    private String jugarCodebreaker(String candidat) throws Exception{
        int dif = partidaActual.getdificultat();
        if(dif == 0){
            if(candidat.length() != 3) throw new Exception("Llargada no valida");
            for(int i = 0; i < 3; ++i) if(candidat.charAt(i) > '4' || candidat.charAt(i) < '0') throw new Exception("Color no valid");
        }
        if(dif == 1){
            if(candidat.length() != 4) throw new Exception("Llargada no valida");
            for(int i = 0; i < 4; ++i) if(candidat.charAt(i) > '5' || candidat.charAt(i) < '0') throw new Exception("Color no valid");
        }
        if(dif == 2){
            if(candidat.length() != 5) throw new Exception("Llargada no valida");
            for(int i = 0; i < 5; ++i) if(candidat.charAt(i) > '7' || candidat.charAt(i) < '0') throw new Exception("Color no valid");
        }
        return partidaActual.novajugadacodebreaker(candidat);
    }
    
    //Post: Realitza una jugada Codemaker
    private String jugarCodemaker(String bn)throws Exception{ 
        int dif = partidaActual.getdificultat();
        if(dif == 0){
            if(bn.length() > 3) throw new Exception("Llargada no valida");
        }
        if(dif == 1){
            if(bn.length() > 4) throw new Exception("Llargada no valida");
        }
        if(dif == 2){
            if(bn.length() > 5) throw new Exception("Llargada no valida");
        }
        for(int i = 0; i < bn.length(); ++i) if(bn.charAt(i) != 'B' && bn.charAt(i) != 'N') throw new Exception("Nomes B o N");
        return partidaActual.novajugadacodemaker(bn);
    }

    //Post: finalitza una ronda i si el mode es competitiu n'inicia la seguent
    public void finalitzarRonda(){ //genera la ronda seguent o va a finalitza partida 
        if(partidaActual.getmode().compareTo("practica") == 0) finalitzarPartida();
        else{
            if(partidaActual.getNrondes() < 4 && partidaActual.getRolActual() == 0){
                partidaActual.novarondacodebreaker();
                String aGuardar = partidaActual.PartidatoString();
                Controlador_Persistencia.modifyLine(aGuardar, idxPartida);
            }
            else if(partidaActual.getNrondes() == 4) finalitzarPartida();
        }
    }
    
    //Post: Finalitza una partida esborrantla de memoria, afegint la puntuacio
    //      al ranking si es necessari i posant a null el punter de partida
    //      actual.
    private void finalitzarPartida(){ //si era competicio s'intenta collocar la puntuacio al ranking
    //la puntuació encara no está implementada i el ranking no forma part de la jugabilitat 
    //aixi que estará comentat

        if(partidaActual.getmode().compareTo("competitiu") == 0){
            int puntuacio = partidaActual.calcularPuntuacio();
            String rank;
            String nom = partidaActual.getnom();
            if(partidaActual.getdificultat() == 0){
                if(ranking.setFacil(puntuacio, nom)){
                    rank = ranking.getStrFacil();
                    Controlador_Persistencia.storeRanking(rank, 0);
                }
            }
            else if(partidaActual.getdificultat() == 1){
                if(ranking.setMitjana(puntuacio, nom)){
                    rank = ranking.getStrMitjana();
                    Controlador_Persistencia.storeRanking(rank, 1);
                }
            }
            else {
                if(ranking.setDificil(puntuacio, nom)){
                    rank = ranking.getStrDificil();
                    Controlador_Persistencia.storeRanking(rank, 2);
                }

            }
        }
        partidaActual = null;

        Controlador_Persistencia.removeLine(idxPartida);
        idxPartida = null;
    }

    //Post: demana a persistencia la partida amb id idx i la reestableix.
    public String carregarPartida(String idx){  
        idxPartida = idx;
        String partidaStr = Controlador_Persistencia.readLine(idx);
        partidaActual = new Partida(partidaStr);
		return partidaActual.getRonda(partidaActual.getNrondes()-1).RondatoPresentacio();
    }

    //Post: esborra la partida amb id idx de memoria
    public void esborrarPartida(String idx){ 
        Controlador_Persistencia.removeLine(idx);
    }

    //Post: retorna el nombre de rondes completes de la partida actual
	public int getrondesacabades(){
		return partidaActual.getNrondes()-1;
	}
    
    //Post: retorna el rol actual de la partida actual
	public int getRolActual(){
		return partidaActual.getRolActual();
	}
    
    //Post: retona el nom de la partida actual
	public String getNombre(){
		return partidaActual.getnom();
	}
    
    //Post: retorna el mode de la partida actual
	public String getModo(){
		return partidaActual.getmode();
	}
    
    //Post: retorna la dificultat de la partida actual
	public int getDificultad(){
		return partidaActual.getdificultat();
	}
    
    //Post: retorna un String amb el Ranking facil
	public String getStrFacil (){
        return ranking.getStrFacil();
    }

    //Post: retorna un String amb el Ranking mitja
    public String getStrMitjana (){
        return ranking.getStrMitjana();
    }

    //Post: retorna un String amb el Ranking dificil
    public String getStrDificil (){
		return ranking.getStrDificil();
	}

}