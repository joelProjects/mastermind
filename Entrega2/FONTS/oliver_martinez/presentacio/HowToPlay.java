/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mastermind.presentacio;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.stage.Stage;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import java.lang.String;
import javafx.scene.control.ListView;
import java.io.*;
import java.util.*;
import java.util.ArrayList;
import javax.swing.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

import javafx.collections.ObservableList;



/**
 *
 * @author Olive
 */
public class HowToPlay implements Initializable {
    
    // @FXML
    // private void handlecrea(ActionEvent event) throws Exception{
		// if(list.getFocusModel().getFocusedItem() != null){
			// ControladorPresentacio.carregarPartida(list.getFocusModel().getFocusedItem());
			// ControladorPresentacio.nueva=false;	
			// Parent home_page_parent = FXMLLoader.load(getClass().getResource("Taula.fxml"));
			// Scene home_page_scene = new Scene(home_page_parent);
			// Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			// app_stage.setResizable(true);
			// app_stage.setScene(home_page_scene);
			// app_stage.show();
		// }
    // }
	
	
    
    @FXML
    private void handleatras(ActionEvent event) throws Exception{
        Parent home_page_parent = FXMLLoader.load(getClass().getResource("Inicio.fxml"));
        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.setScene(home_page_scene);
        app_stage.setResizable(false);
        app_stage.show();  
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) { 
		
    }    
    
}
