/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mastermind.presentacio;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.stage.Stage;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import java.lang.String;
import javafx.scene.layout.Pane;
import javafx.event.EventHandler;
import javafx.scene.input.KeyEvent;



/**
 *
 * @author Olive
 */
public class NuevaPartida implements Initializable {
    
   
	@FXML
    private Text exc;
	@FXML
    private TextField nombre;
    @FXML
    private ChoiceBox<String> dificultad;
    @FXML
    private ChoiceBox<String> modo;
    @FXML
    private ChoiceBox<String> rol;
     
    
    @FXML
    private void handlecrea(ActionEvent event) throws Exception{
        if(! nombre.getText().trim().equals("") && nombre.getText().matches("[^\\%\\?\\[\\(]*")) {
            String str= nombre.getText();
            String dif= dificultad.getSelectionModel().getSelectedItem();
            String m= modo.getSelectionModel().getSelectedItem();
            String r= rol.getSelectionModel().getSelectedItem();
			ControladorPresentacio.nombre = str;
			int diff;
			if(dif.equals("Facil")) diff=0;
            else if(dif.equals("Normal")) diff=1;
            else diff=2;
			ControladorPresentacio.dificultad = diff;
			if(m.equals("Practica")) ControladorPresentacio.modo="practica";
            else ControladorPresentacio.modo="competitiu";
			if(r.equals("CodeMaker")) ControladorPresentacio.rol=0;
            else ControladorPresentacio.rol=1;
			if(ControladorPresentacio.crearPartida()){	
				ControladorPresentacio.nueva=true;	
				ControladorPresentacio.rondesacabades=0;
				try{
						Parent home_page_parent = FXMLLoader.load(getClass().getResource("Taula.fxml"));
						Scene home_page_scene = new Scene(home_page_parent, 920.0, 580.0);
						Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
						app_stage.setScene(home_page_scene);
                        app_stage.setResizable(true);
                        app_stage.setWidth(920.0);
                        app_stage.setHeight(580.0);
						app_stage.show(); 
						
				} catch (Exception exc){
					System.out.println("per provar, " + exc.getMessage());
				}
			}
			else{
				exc.setText("Limite de partidas alcanzado");
			}
        }
    }
	
    
    @FXML
    private void handleatras(ActionEvent event) throws Exception{
        Parent home_page_parent = FXMLLoader.load(getClass().getResource("Inicio.fxml"));
        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.setScene(home_page_scene);
        app_stage.show();  
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) { //https://coderanch.com/t/649781/java/Set-values-ChoiceBox-created-Scene
        nombre.addEventFilter(KeyEvent.KEY_TYPED , letter_Validation(50));
    }

    public EventHandler<KeyEvent> letter_Validation(final Integer max_Lengh) {
    return new EventHandler<KeyEvent>() {
        @Override
        public void handle(KeyEvent e) {
            TextField txt_TextField = (TextField) e.getSource();                
            if (txt_TextField.getText().length() >= max_Lengh) {                    
                e.consume();
            }
            if(e.getCharacter().matches("[^\\%\\?\\[\\(]")){ 
            }else{
                e.consume();
            }
        }
    };
}    
  
}
