package mastermind.presentacio;

import javafx.application.Application;
import mastermind.domini.ControladorDomini;


public class ControladorPresentacio {
	
	public static ControladorDomini CD;
	
	public static int dificultad;
	
	public static int rol;
	
	public static String modo;
	
	public static String nombre;
	
	public static boolean nueva;
	
	public static String PartidaCargada;
	
	public static int rondesacabades;
	
    public static void main(String[] args){
		CD = new ControladorDomini();
        Application.launch(Menu.class, args);
        

    }
	
	public static String getIndexs(){
		return CD.sendIndexs();
	}
	
	public static boolean crearPartida() throws Exception{
		return CD.crearPartida(nombre, dificultad, modo, rol);
	}
	
	public static String creaRondaCodemaker(String solucio) throws Exception{
		return CD.creaRondaCodemaker(solucio,nombre);
	}
	
	public static String jugar(String input) throws Exception{
		return CD.jugar(input);
	}
	
	public static void finalitzarRonda(){
		++rondesacabades;
		CD.finalitzarRonda();
	}
	
	public static void carregarPartida(String idx){  
		PartidaCargada = CD.carregarPartida(idx);
		rondesacabades= CD.getrondesacabades();
		nombre= CD.getNombre();
		dificultad= CD.getDificultad();
		modo= CD.getModo();
		rol= CD.getRolActual();
	}
	
	public static void esborrarPartida(String idx){ 
		CD.esborrarPartida(idx);
	}
}