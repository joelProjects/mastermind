package mastermind.persistencia;
/**
 * 
 * @author Joan Llop
 */


import java.io.FileReader;
import java.io.FileWriter;
import java.io.File;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.*;

public class Controlador_Persistencia {


    public static boolean writeLine (String line, String index) {
        try {
            FileWriter writer = new FileWriter("mastermind/persistencia/indexs.txt", true);
            writer.write(index+"\n");
            writer.close();

            FileWriter writer2 = new FileWriter("mastermind/persistencia/partides.txt", true);
            String index_line = index + line+"\n";
            writer2.write(index_line);
            writer2.close();
            return true;
        }
        catch (IOException e) {
            return false;
        }
    }

    public static String readLine (String index){
        try {
            FileReader reader = new FileReader("mastermind/persistencia/partides.txt");

            BufferedReader lr = new BufferedReader(reader);

            int beginIndex = 0;
            int endIndex = index.indexOf('|');
            index = index.substring(0,index.indexOf('|'));

            String s = lr.readLine();
            while ((s != null) && (! s.substring(beginIndex, endIndex).equals(index))) s = lr.readLine();

            // System.out.println(s);
            lr.close();
            reader.close();
            if (s != null) return s.substring(s.indexOf('['), s.length());
            else return "-1";
        }
        catch (IOException e) {
            return "-1";
        }
    }

    public static String getIndexs () {
        try {
            FileReader reader = new FileReader("mastermind/persistencia/indexs.txt");
            BufferedReader lr = new BufferedReader(reader);
            String s, r;
            s = r = "";
            while ((s = lr.readLine()) != null) r += s+"\n";
            lr.close();
            String[] aux = r.split("\n");
            List<String> list_aux = Arrays.asList(aux);
            Collections.reverse(list_aux);
            aux = (String[]) list_aux.toArray();
            r = String.join("\n", aux);
            return r;
        }
        catch (IOException e) {
            return "-1";
        }
    }

    public static boolean removeLine(String index){
        try {
            File inputIndexs = new File("mastermind/persistencia/indexs.txt");
            File tempIndexs = new File("mastermind/persistencia/tempIndexs.txt");
            BufferedReader lr = new BufferedReader(new FileReader(inputIndexs));
            FileWriter lw = new FileWriter(tempIndexs);
            String s;
            String removedLine = index;
            while ((s = lr.readLine()) != null){
                if (!s.equals(removedLine)) lw.write(s+"\n");
            }

            lr.close();
            lw.close();

            inputIndexs.delete();
            boolean succ = tempIndexs.renameTo(inputIndexs);

            // ---------------------------------------------

            removedLine = removedLine.substring(0,removedLine.indexOf('|'));
            File inputPartides = new File("mastermind/persistencia/partides.txt");
            File tempPartides = new File("mastermind/persistencia/tempPartides.txt");

            BufferedReader pr = new BufferedReader(new FileReader(inputPartides));
            FileWriter pw = new FileWriter(tempPartides);

            int beginIndex = 0;
            int endIndex = index.length();
            
            while ((s = pr.readLine()) != null){
                if (!s.substring(0,s.indexOf('|')).equals(removedLine)) pw.write(s+"\n");
            }

            pr.close();
            pw.close();

            inputPartides.delete();
            boolean succ2 = tempPartides.renameTo(inputPartides);

            return succ && succ2;


        }
        catch (IOException e) {
            return false;
        }
    }

    public static boolean modifyLine (String newLine, String index){
        if (removeLine(index)) {
            writeLine(newLine, index);
            return true;
        }
        else{
            return false;
        }
    }

    public static boolean storeRanking (String Ranking, int diff) {
        try {
            if (diff == 0){
                File tempRankingE = new File("mastermind/persistencia/tempRankingE.txt");
                FileWriter writer = new FileWriter(tempRankingE);
                writer.write(Ranking);
                writer.close();
                (new File("mastermind/persistencia/RankingE")).delete();
                if (tempRankingE.renameTo(new File("mastermind/persistencia/RankingE"))){
                    return true;
                }
                else return false;
            }
            else if (diff == 1){
                File tempRankingM = new File("mastermind/persistencia/tempRankingM.txt");
                FileWriter writer = new FileWriter(tempRankingM);
                writer.write(Ranking);
                writer.close();
                (new File("mastermind/persistencia/RankingM")).delete();
                if (tempRankingM.renameTo(new File("mastermind/persistencia/RankingM"))){
                    return true;
                }
                else return false;
            }
            else if (diff == 2){
                File tempRankingH = new File("mastermind/persistencia/tempRankingH.txt");
                FileWriter writer = new FileWriter(tempRankingH);
                writer.write(Ranking);
                writer.close();
                (new File("mastermind/persistencia/RankingH")).delete();
                if (tempRankingH.renameTo(new File("mastermind/persistencia/RankingH"))){
                    return true;
                }
                else return false;
            }
            else return false;
        }
        catch (IOException e) {
            return false;
        }
    }

    public static String loadRanking(int diff){
        try {
            File reader;
            if (diff == 0) reader = new File("mastermind/persistencia/RankingE");
            else if (diff == 1) reader = new File("mastermind/persistencia/RankingM");
            else reader = new File("mastermind/persistencia/RankingH");

            if (!reader.exists()){
                reader.createNewFile();
            }
            BufferedReader lr = new BufferedReader(new FileReader(reader));
            String s, r;
            s = r = "";
            while ((s = lr.readLine()) != null) r += s;
            lr.close();
            return r;
        }
        catch (IOException e) {
            // System.out.println(e.getMessage());
            return "-1";
        }
    }


    // public static void main(String[] args) {
    //     // System.out.println(storeRanking("ranckingH2", 2));
    //     // System.out.println(loadRanking(0));

    //     // System.out.println(getIndexs());
    //     // System.out.println(writeLine("3333", "3"));
    //     // System.out.println(writeLine("qwerty1234", "qw"));
    //     // System.out.println(readLine("qw"));

    //     // System.out.println(modifyLine("qwerty4321", "qw"));
    //     // System.out.println(removeLine("3"));

    // }

    
} 