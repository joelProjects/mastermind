package mastermind.domini;

/**
 * 
 * @author Joel Moreno
 */

public class Posicion{
    public String nombre;     //Nombre de la partida
    public int puntuacion;  //Puntuacion de la partida
    
    public Posicion(String name, int score){
        nombre = name;
        puntuacion = score;
    }
}