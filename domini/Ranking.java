package mastermind.domini;


/**
 * 
 * @author Joel Moreno
 */


import java.util.ArrayList;

public class Ranking {
    
    private ArrayList<Posicion> facil;
    private ArrayList<Posicion> mitjana;
    private ArrayList<Posicion> dificil;

    //Pre: Els strings han de tenir l'estructura adient per a refer els rankings
    //Post: restableix els tres rankings per dificultats desde els strings
    //      arribats.
    public Ranking(String easy, String medium, String hard){
        String nom;
        int punts;
        facil = new ArrayList<Posicion>();
        while(!easy.equals("")){
            nom = easy.substring(0, easy.indexOf("|"));
            easy = easy.substring(easy.indexOf("|") + 1);
            punts = Integer.parseInt(easy.substring(0, easy.indexOf("|")));
            easy = easy.substring(easy.indexOf("|") + 1);
            facil.add(new Posicion(nom, punts));
        }


        mitjana = new ArrayList<Posicion>();
        while(!medium.equals("")){
            nom = medium.substring(0, medium.indexOf("|"));
            medium = medium.substring(medium.indexOf("|") + 1);
            punts = Integer.parseInt(medium.substring(0, medium.indexOf("|")));
            medium = medium.substring(medium.indexOf("|") + 1);
            mitjana.add(new Posicion(nom, punts));
        }


        dificil = new ArrayList<Posicion>();
        while(!hard.equals("")){
            nom = hard.substring(0, hard.indexOf("|"));
            hard = hard.substring(hard.indexOf("|") + 1);
            punts = Integer.parseInt(hard.substring(0, hard.indexOf("|")));
            hard = hard.substring(hard.indexOf("|") + 1);
            dificil.add(new Posicion(nom, punts));
        }
    }

    //Post: retorna el ranking facil
    public ArrayList<Posicion> getFacil(){
        return facil;
    }

    //Post: retorna el ranking mitja
    public ArrayList<Posicion> getMitjana(){
        return mitjana;
    }

    //Post: retorna el ranking dificil
    public ArrayList<Posicion> getDificil(){
        return dificil;
    }

    //Post: afegeix, si es adient, la nova partida puntuada al ranking
    public Boolean setFacil(int score, String name){
        int pos = -1;
        for(int i = 0; i < facil.size() && pos == -1; ++i){
            if(facil.get(i).puntuacion < score) pos = i;
        }
        if(pos > -1){
            facil.add(pos, new Posicion(name, score));
            if(facil.size() > 10) facil.remove(10);
            return true;
        }
        else if(pos == -1 && facil.size() < 10){
            facil.add(new Posicion(name, score));
            return true;
        }
        return false;
    }

    //Post: afegeix, si es adient, la nova partida puntuada al ranking
    public Boolean setMitjana(int score, String name){
        int pos = -1;
        for(int i = 0; i < mitjana.size() && pos == -1; ++i){
            if(mitjana.get(i).puntuacion < score) pos = i;
        }
        if(pos > -1){
            mitjana.add(pos, new Posicion(name, score));
            if(mitjana.size() > 10) mitjana.remove(10);
            return true;
        }
        else if(pos == -1 && mitjana.size() < 10){
            mitjana.add(new Posicion(name, score));
            return true;
        }
        return false;
    }

    //Post: afegeix, si es adient, la nova partida puntuada al ranking
    public Boolean setDificil(int score, String name){
        int pos = -1;
        for(int i = 0; i < dificil.size() && pos == -1; ++i){
            if(dificil.get(i).puntuacion < score) pos = i;
        }
        if(pos > -1){
            dificil.add(pos, new Posicion(name, score));
            if(dificil.size() > 10) dificil.remove(10);
            return true;
        }
        else if(pos == -1 && dificil.size() < 10){
            dificil.add(new Posicion(name, score));
            return true;
        }
        return false;
    }

    //Post: retorna un string de tot el ranking facil
    public String getStrFacil (){
        String strRanking = "";
        for(int i = 0; i < facil.size(); ++i){
            Posicion pos = facil.get(i);
            String score = Integer.toString(pos.puntuacion);
            strRanking = strRanking.concat(pos.nombre).concat("|").concat(score).concat("|");
        }
        return strRanking;
    }

    //Post: retorna un string de tot el ranking mitja
    public String getStrMitjana (){
        String strRanking = "";
        for(int i = 0; i < mitjana.size(); ++i){
            Posicion pos = mitjana.get(i);
            String score = Integer.toString(pos.puntuacion);
            strRanking = strRanking.concat(pos.nombre).concat("|").concat(score).concat("|");
        }
        return strRanking;
    }

    //Post: retorna un string de tot el ranking dificil
    public String getStrDificil (){
        String strRanking = "";
        for(int i = 0; i < dificil.size(); ++i){
            Posicion pos = dificil.get(i);
            String score = Integer.toString(pos.puntuacion);
            strRanking = strRanking.concat(pos.nombre).concat("|").concat(score).concat("|");
        }
        return strRanking;
    }
}