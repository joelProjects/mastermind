package mastermind.domini;



import java.util.ArrayList;
import java.lang.Character;

/**
 *
 * @author Oliver
 */
public class Partida {
    private final String mode; //competitiu | practica
    private final int dificultat; //0 1 2
    private ArrayList<Ronda> rondes;
    private final int rolInicial; //0 CM 1 CB
    private final String nom;
	private int puntuacio;
    
    public Partida(String m, int dif, int rol, String n){
        mode=m;
        dificultat=dif;
        rolInicial=rol;
        rondes= new ArrayList<>();
        nom=n;
		puntuacio=0;
    }
	
	public String PartidatoString(){
		String res = "";
		if(rondes.size()>0)
			res += rondes.get(rondes.size()-1).RondatoString();
		
		int modo;
		if(mode.equals("practica"))modo=0;
		else modo=1;
		return "["+modo+dificultat+rolInicial+nom+"("+puntuacio+"?"+rondes.size()+"%"+res+")]";
	}
	
	public Partida(String str){
		rondes= new ArrayList<>();
		int modo= Character.digit(str.charAt(1),10);
		if(modo==0)mode="practica";
		else mode="competitiu";
		dificultat= Character.digit(str.charAt(2),10);
		rolInicial= Character.digit(str.charAt(3),10);
		nom = str.substring(4, str.indexOf('('));
		puntuacio = Integer.parseInt(str.substring(str.indexOf('(')+1, str.indexOf('?')));
		int nrondas= Integer.parseInt(str.substring(str.indexOf('?')+1, str.indexOf('%')));
		if(nrondas>0){
			Ronda new_ronda = new Ronda();
			for(int i=0;i<nrondas-1;++i){
				rondes.add(new_ronda); 
			}
			Ronda newronda;
			String solucio;
			if((rondes.size() + rolInicial) % 2 ==0) {
				newronda = new RondaCodeMaker(dificultat);
				str= str.substring(str.indexOf('%')+1,str.length());
				((RondaCodeMaker) newronda).setPreviousCandidates(str.substring(0,str.indexOf('_')));
				str= str.substring(str.indexOf('_')+1,str.length());
				((RondaCodeMaker) newronda).setPreviousBN(str.substring(0,str.indexOf('_')));
				str= str.substring(str.indexOf('_')+1,str.length());
				solucio= str.substring(0,str.indexOf('|'));
				((RondaCodeMaker) newronda).setSolution(solucio);
				str= str.substring(str.indexOf('|')+1,str.length());
			}
			else {
				str= str.substring(str.indexOf('%')+1,str.length());
				solucio= str.substring(0,str.indexOf('|'));
				str= str.substring(str.indexOf('|')+1,str.length());
				newronda = new Ronda(dificultat,solucio);
			}
			newronda.afegirJugades(str);
			rondes.add(newronda);
		}
	}
    
    public String novarondacodemaker(String solucion) throws Exception{
        RondaCodeMaker new_ronda = new RondaCodeMaker(dificultat);
        new_ronda.setSolution(solucion);
        new_ronda.getNextCandidate(); 
        rondes.add(new_ronda);   
        return new_ronda.getjugada(new_ronda.getNjugades()-1).getCandidat();
    }

    public void novarondacodebreaker(){
		puntuacio+=12;
        Ronda new_ronda = new Ronda(dificultat);
        rondes.add(new_ronda); 
    }
    
    public String novajugadacodebreaker(String candidat) throws Exception{
		--puntuacio;
        Ronda ronda_actual = rondes.get(rondes.size() - 1);
        ronda_actual.afegirJugada(candidat);
        return ronda_actual.getjugada(ronda_actual.getNjugades() - 1).getBN();
    }
    
    public String novajugadacodemaker(String BN) throws Exception{
		++puntuacio;
        RondaCodeMaker ronda_actual = (RondaCodeMaker) rondes.get(rondes.size() - 1);
		int N = 0;
        int B = 0;
        for (int i = 0; i < BN.length(); ++i) {
            if (BN.charAt(i) == 'N') ++N;
            else ++B;
        }
		String NB= ronda_actual.getjugada(ronda_actual.getNjugades() - 1).getBN();
        for (int i = 0; i < NB.length(); ++i) {
            if (NB.charAt(i) == 'N') --N;
            else --B;
        }
        if(N!=0 || B!=0) throw new Exception("El jugador no esta introduciendo las BN correctamente");
        ronda_actual.getNextCandidate();
        return ronda_actual.getjugada(ronda_actual.getNjugades()-1).getCandidat();
    }
    
    public Ronda getRonda(int i){
        if(i>=0 && i<rondes.size()) return rondes.get(i);
        throw new IndexOutOfBoundsException() ;
    }
    
    public int calcularPuntuacio(){
        if(mode.equals("practica")) return 0;
        return puntuacio;
    }
    
    public int getNrondes(){
        return rondes.size();
    }
    
    public String getmode(){
        return mode;
    }
    
    public int getdificultat(){
        return dificultat;
    }
    
    public int getRolActual(){
		if(rondes.size()==0) throw new RuntimeException();
        return (rondes.size()+1 + rolInicial) % 2;
    }

    public int getRolInicial(){
        return rolInicial;
    }
    
    public String getnom(){
        return nom;
    }
}
