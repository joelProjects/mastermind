package mastermind.domini;
/**
 * 
 * @author Joan Llop
 */


public class RondaCodeMaker extends Ronda{

    private static final int MAX_ZERO = 200;

    private int NUM_COLORS = 8;
    private int NUM_HOLES = 5; // 1 <= NUM_HOLES <= 9

    public String previousCandidates = "";
    public String previousBN = "";

	//Oliver did this
	public String RondatoString(){
		String res = "|";
		for(int i=0;i<jugades.size();++i){
			res += jugades.get(i).JugadatoString();
		}
		return getPreviousCandidates()+"_"+getPreviousBN()+"_"+solucio+res+"|";
	}
	//
	
    public RondaCodeMaker (int dif){

        dificultat=dif;
        if (dif == 0){
            NUM_COLORS = 5;
            NUM_HOLES = 3;
        }
        else if (dif == 1){
            NUM_COLORS = 6;
            NUM_HOLES = 4;
        }
        else{
            NUM_COLORS = 8;
            NUM_HOLES = 5;
        }
    }

    /* Getters and Setters*/

    public void setSolution(String S){
        solucio = new Individual(S, NUM_COLORS);
    }

    public void setNumColors(int nC){
        NUM_COLORS = nC;
    }

    public void setNumHoles(int nH){
        NUM_HOLES = nH;
    }

    public int getNumColors(){
        return NUM_COLORS;
    }

    public int getNumHoles(){
        return NUM_HOLES;
    }

    public void setBN (String BN, String candidate){
        int N = 0;
        int B = 0;

        for (int i = 0; i < BN.length(); ++i) {
            if (BN.charAt(i) == 'N') ++N;
            else ++B;
        }

        Individual indiv = new Individual(candidate, NUM_COLORS);

        FitnessCalc.addPreviousCandidate(indiv, N, B);
    }

    public String getNextCandidate() throws Exception{
        FitnessCalc.setBN(previousBN);
        FitnessCalc.setCandidates(previousCandidates);
        String Candidate = "";
        if (FitnessCalc.Empty()){
            if (dificultat == 0) Candidate = "001";
            else if (dificultat == 1) Candidate = "0012";
            else Candidate = "00123";
        }
        else {
            boolean izm = true;
            while (izm){
                Population MyPop = Algorithm.genetic_evolution(NUM_COLORS, NUM_HOLES);
                int n_zero = 0;
                while (MyPop.size() == 0 && n_zero < MAX_ZERO){
                    MyPop = Algorithm.genetic_evolution(NUM_COLORS, NUM_HOLES);
                    ++n_zero;
                }

                int i = MyPop.size()-1;

                while (i >= 0 && FitnessCalc.is_Indiv_in_previous_guesses(MyPop.getIndividual(i))) {
                    --i;
                }
                /*if (i < 0) {
                    Individual Ca = new Individual(jugades.get(jugades.size()-1).getCandidat(), NUM_COLORS);
                    Algorithm.noCandidate(Ca);
                    Candidate = Ca.toString();
                }
                else*/
                if (i > 0){
                    izm = false;
                    Candidate = MyPop.getIndividual(i).clone().toString();
                }
                else{
                    ++Algorithm.MIN_Q;
                }
            }
            Algorithm.MIN_Q = 0;
        }

        afegirJugada(Candidate);

        String BN = jugades.get(jugades.size()-1).getBN();
        this.setBN(BN, Candidate);
        previousCandidates = FitnessCalc.getCandidates();
        previousBN = FitnessCalc.getBN();
        return Candidate;
    }

    public String getPreviousCandidates(){
        return previousCandidates;
    }

    public String getPreviousBN(){
        return previousBN;
    }

    public void setPreviousBN(String pBN){
        previousBN = pBN;
    }

    public void setPreviousCandidates(String pC){
        previousCandidates = pC;
    }



}