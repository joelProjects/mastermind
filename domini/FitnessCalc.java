package mastermind.domini;

import java.util.*;

/**
 * 
 * @author Joan Llop
 */

public class FitnessCalc {

    private static ArrayList<Individual> previousCandidates = new ArrayList<Individual>();
    private static ArrayList<int[]> previous_NB = new ArrayList<int[]>();

    /* Public methods */
    public static void addPreviousCandidate(Individual Indiv, int N, int B) {
        Individual copy_Indiv = Indiv.clone();
        previousCandidates.add(copy_Indiv);
        int[] NB = new int[2];
        NB[0] = N;
        NB[1] = B;
        previous_NB.add(NB);

    }

    private static int check_diffs (Individual individual, Individual previousCandidate){
        int NUM_COLORS = individual.getNumColors();
        byte[] copy_ai = new byte[individual.size()];
        byte[] copy_pr = new byte[previousCandidate.size()];

        Integer placeN = 0;
        Integer placeB = 0;

        ArrayList<Boolean> es_pr = new ArrayList<Boolean> ();
        for (int i = 0; i < NUM_COLORS; ++i){
            es_pr.add(i, false);
        }

        for (int i = 0; i < individual.size(); ++i){
            copy_ai[i] = individual.getGene(i);
            copy_pr[i] = previousCandidate.getGene(i);
            if (copy_ai[i] == copy_pr[i]) {
                placeN += 1;
                copy_ai[i] = 50;
                copy_pr[i] = 55;
            }
            else{
                es_pr.set(copy_pr[i], true);
            }
        }

        

        for (int i = 0; i < copy_ai.length; ++i){
            if (copy_ai[i] != 50){
                if (es_pr.get(copy_ai[i])){
                    placeB += 1;
                    es_pr.set(copy_ai[i], false);
                }
            }
        }

        return placeN*10 + placeB;
    }
    
    public static Boolean is_Indiv_in_previous_guesses (Individual indiv){
        for (int i = 0; i < previousCandidates.size(); ++i) {
            if (indiv.equals(previousCandidates.get(i))) return true;
        }
        return false;
    }

    public static Boolean Empty(){
        return previous_NB.size() == 0;
    }

    /* Getters and Setters*/
    
    public static int getFitness(Individual individual) {
        int fitness = 0;
        for (int i = 0; i < previousCandidates.size(); i++) {
            Integer placeN = 0;
            Integer placeB = 0; 
            int r = check_diffs(individual, previousCandidates.get(i));
            placeN = r / 10;
            placeB = r % 10;
            fitness += Math.abs(placeN-previous_NB.get(i)[0]) + Math.abs(placeB-previous_NB.get(i)[1]);
        }

        return fitness;
    }

    public static String getBN(){
        String r = "";
        for (int i = 0; i < previous_NB.size(); ++i){
            r += previous_NB.get(i)[0];
            r += previous_NB.get(i)[1];
        }
        return r;
    }

    public static void setBN(String BN){
        previous_NB = new ArrayList<int[]>();
        for (int i = 0; i < BN.length()-1; i+=2){
            int[] NB = new int[2];
            NB[0] = (int) (BN.charAt(i) - '0');
            NB[1] = (int) (BN.charAt(i+1) - '0');
            previous_NB.add(NB);
        }
    }

    public static String getCandidates(){
        String r = "";
        for (int i = 0; i < previousCandidates.size(); ++i){
            r += previousCandidates.get(i).toString();
            r += "|";
        }
        return r;
    }

    public static void setCandidates(String Cn){
        previousCandidates = new ArrayList<Individual>();
        String Candidate = "";
        for (int i = 0; i < Cn.length(); ++i){
            if (Cn.charAt(i) == '|'){
                int NUM_COLORS;
                if (Candidate.length() == 3) NUM_COLORS = 5;
                else if (Candidate.length() == 4) NUM_COLORS = 6;
                else NUM_COLORS = 8;
                Individual indiv = new Individual(Candidate, NUM_COLORS);
                previousCandidates.add(indiv);
                Candidate = "";
            }
            else{
                Candidate += Cn.charAt(i);
            }
        }        
    }
}
