package test.testAlgorithm;

/**
 * 
 * @author Joan Llop
 */

import java.util.*;

public class DriverAlgorithm {

	private static Scanner reader = new Scanner(System.in);

	public static void main (String[] args){
		usage();
		boolean a = true;
		int op = 8;
		while (a) {
			try {
				System.out.println("\nIntroduiu opcio: ");
				op = reader.nextInt();
				a = false;
			}
			catch (Exception e){
				System.out.println("Exception: " + e.getMessage() + "\n\tl'opcio ha de ser un numero");
			}
		}
		while (op != 6){
			switch(op){
				case 1:
					if (testGenetic_evolution()) System.out.println("==========ok==========");
					else System.out.println("==========TEST FAIL==========");
				break;
				case 2:
					if (testCrossover()) System.out.println("==========ok==========");
					else System.out.println("==========TEST FAIL==========");
				break;
				case 3:
					if (testMutate()) System.out.println("==========ok==========");
					else System.out.println("==========TEST FAIL==========");
				break;
				case 4:
					if (testPermute()) System.out.println("==========ok==========");
					else System.out.println("==========TEST FAIL==========");
				break;
				default:
				usage();
			}
			a = true;
			while (a) {
				try {
					System.out.println("\nIntroduiu opcio: ");
					op = reader.nextInt();
					a = false;
				}
				catch (Exception e){
					System.out.println("Exception: " + e.getMessage() + "\n\tl'opcio ha de ser un numero");
				}
			}
		}
	}

	private static boolean testGenetic_evolution () {//serveix per comprovar si esta + 60 individus o 0...
		boolean r = true;		
		try {
			String Pre = "Pre: llargada del codi < 10\n";
			Pre += "\tnumero de colors < 10\n";
			System.out.println("Provant Genetic_evolution...\n" + Pre);
			System.out.println(">>>Introduiu el numero de colors: ");
			int nC = reader.nextInt();
			System.out.println(">>>Introduiu la llargada del codi a generar: ");
			int nH = reader.nextInt();
			Population myPop = Algorithm.genetic_evolution(nC, nH);
			for (int i = 0; i < myPop.size(); ++i){
				System.out.println("\t\t" + myPop.getIndividual(i).toString());
			}
		}
		catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
			r = false;
		}
		return r;
	}

	private static boolean testCrossover() {
		boolean r = true;		
		try {
			String Pre = "Pre: els codis tenen la mateixa llargada i el mateix num_colors\n";
			Pre += "\tllargada del codi < 10\n";
			Pre += "\tnumero de colors < 10\n";
			System.out.println("Provant Crossover...\n" + Pre);
			System.out.println(">>>Introduiu el numero de colors del primer individu: ");
			int nC1 = reader.nextInt();
			System.out.println(">>>Introduiu el codi del primer candidat(Exemple: 1234): ");
			String Cod1 = reader.next();
			Individual indiv1= new Individual(Cod1, nC1);

			System.out.println(">>>Introduiu el numero de colors del segon individu: ");
			int nC2 = reader.nextInt();
			System.out.println(">>>Introduiu el codi del segon individu(Exemple: 1234): ");
			String Cod2 = reader.next();
			Individual indiv2= new Individual(Cod2, nC2);
			System.out.println("Individu resultant de creuar els dos primers: " + Algorithm.crossover(indiv1, indiv2).toString());
		}
		catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
			r = false;
		}
		return r;
	}

	private static boolean testMutate() {
		boolean r = true;		
		try {
			String Pre = "Pre: llargada del codi < 10\n";
			Pre += "\tnumero de colors < 10\n";
			System.out.println("Provant mutate...\n" + Pre);
			System.out.println(">>>Introduiu el numero de colors: ");
			int nC = reader.nextInt();
			System.out.println(">>>Introduiu el codi (Exemple: 1234): ");
			String Cod = reader.next();
			Individual indiv= new Individual(Cod, nC);

			Algorithm.mutate(indiv);
			System.out.println("Individu despres de mutar: " + indiv.toString());
		}
		catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
			r = false;
		}
		return r;
	}

	private static boolean testPermute() {
		boolean r = true;		
		try {
			String Pre = "Pre: llargada del codi < 10\n";
			Pre += "\tnumero de colors < 10\n";
			System.out.println("Provant permute...\n" + Pre);
			System.out.println(">>>Introduiu el numero de colors: ");
			int nC = reader.nextInt();
			System.out.println(">>>Introduiu el codi (Exemple: 1234): ");
			String Cod = reader.next();
			Individual indiv= new Individual(Cod, nC);

			Algorithm.noCandidate(indiv);
			System.out.println("Individu despres de ser permutat: " + indiv.toString());
		}
		catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
			r = false;
		}
		return r;
	}

	private static void usage (){
		System.out.println("\nUsage:\n1 - test genetic evolution\n2 - test crossover\n3 - test mutate\n4 - test permute\n5 - usage\n6 - exit\n");
	}
}