package test.testPopulation;

import java.util.*;

/**
 * 
 * @author Joan Llop
 */

public class StubFitnessCalc {

    private static ArrayList<Individual> previousCandidates = new ArrayList<Individual>();
    private static ArrayList<int[]> previous_NB = new ArrayList<int[]>();

    /* Public methods */
    public static void addPreviousCandidate(Individual Indiv, int N, int B) {

    }

    public static int check_diffs (Individual individual, Individual previousCandidate){
        
        return 0;
    }
    
    public static Boolean is_Indiv_in_previous_guesses (Individual indiv){
        
        return false;
    }

    public static Boolean Empty(){
        return true;
    }

    /* Getters and Setters*/
    
    public static int getFitness(Individual individual) {
        return 0;
    }

    public static String getBN(){
        return "";
    }

    public static void setBN(String BN){

    }

    public static String getCandidates(){
        return "";
    }

    public static void setCandidates(String Cn){
        
    }
}
