package test.testGA;

/**
 *
 * @author Oliver  5 3 6 4 8 5
 */
public class Jugada {
    private String BN;
    private final String candidat;
    
    public Jugada(String candi, String solucio){
        candidat=candi;   
        assert solucio.length()==candidat.length();
        BN = new String();
        for(int i=0;i<candidat.length();++i){
            if(candi.charAt(i) == solucio.charAt(i)){
                BN += 'N';                
                if(i==0){
                    solucio= "-" + solucio.substring(1,candidat.length());
                    candi= "z" + candi.substring(1,candidat.length());
                }
                else if(i==candidat.length()-1){
                    solucio=  solucio.substring(0,candidat.length()-1) +"-" ;
                    candi=  candi.substring(0,candidat.length()-1) +"z" ;
                }
                else{
                    solucio = solucio.substring(0,i) + "-" + solucio.substring(i+1,candidat.length());
                    candi = candi.substring(0,i) + "z" + candi.substring(i+1,candidat.length());
                }
            }
        }
        for(int i=0;i<candidat.length();++i){
            
                boolean found=false;
                for(int j=0;j<candidat.length() && !found;++j){
                    if(candi.charAt(i) == solucio.charAt(j)){
                        BN += 'B';
                        found=true;
                        solucio = solucio.replaceFirst(Character.toString(candi.charAt(i)),"-");
                    }
                }
                      
        }
    }    
    
    public String getCandidat(){
        return candidat;
    }
  
    public String getBN(){
        return BN;
    }
}
