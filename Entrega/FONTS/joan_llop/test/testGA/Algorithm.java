package test.testGA;
/**
 * 
 * @author Joan Llop
 */

public class Algorithm {

    /* GA parameters */

    private static final int MAX_POP_SIZE = 60;
    private static final int MAX_GENERATIONS = 100;
    private static int NUM_COLORS;
    private static int NUM_HOLES;
    public static int MIN_Q = 0;

    private static final double CROSSOVER_PROBABILITY = 0.5;
    private static final double CROSSOVER_THEN_MUTATION_PROBABILITY = 0.15;
    private static final double PERMUTATION_PROBABILITY = 0.3;

    /* Public methods */
    
    // Evolve a population
    public static Population genetic_evolution(int nC, int nH) {
        NUM_COLORS = nC;
        NUM_HOLES = nH;
        Population myPopulation = new Population(MAX_POP_SIZE, NUM_COLORS, NUM_HOLES);

        Population result_of_ge = new Population(0, NUM_COLORS, NUM_HOLES);

        int h = 1;
        while (result_of_ge.size() <= MAX_POP_SIZE && h <= MAX_GENERATIONS){
            Population sons = new Population(0, NUM_COLORS, NUM_HOLES);

            Individual indiv_1 = myPopulation.getIndividual(0).clone();

            for (int i = 0; i < myPopulation.size(); ++i){

                if (i == myPopulation.size()-1){
                    sons.addIndividual(myPopulation.getIndividual(i));
                }
                else{
                    Individual indiv_2 = myPopulation.getIndividual(i+1).clone();
                    Individual indiv = new Individual(NUM_COLORS, indiv_2.size());
                    indiv = crossover(indiv_1, indiv_2);

                    indiv_1 = indiv_2;
                    
                    if (Math.random() <= CROSSOVER_THEN_MUTATION_PROBABILITY){
                        mutate(indiv);
                    }

                    permute(indiv);

                    byte[] f = indiv.getGenes();
                    Individual auxx = new Individual(f, NUM_COLORS);

                    sons.addIndividual(auxx);
                    
                }

            }

            sons.sort();


            Population elite = new Population(0, NUM_COLORS, NUM_HOLES);
            int i = 0;
            while (i < sons.size() && sons.getIndividual(i).getFitness() <= MIN_Q){
                elite.addIndividual(sons.getIndividual(i).clone());
                ++i;
            }

            if (elite.size() == 0){
                ++h;
                continue;
            }

            for (i = 0; i < elite.size(); ++i){
                for (int j = 0; j < result_of_ge.size(); ++j){
                    if (elite.getIndividual(i).equals(result_of_ge.getIndividual(j))) {
                        result_of_ge.removeIndividual(j);
                        result_of_ge.addRandom();
                    }
                }
            }


            for (i = 0; i < elite.size(); ++i){
                Boolean b = false;
                for (int j = 0; j < result_of_ge.size(); ++j){
                    if (elite.getIndividual(i).equals(result_of_ge.getIndividual(j))) {
                        b = true;
                    }
                }
                if (!b){
                    result_of_ge.addIndividual(elite.getIndividual(i));
                }
            }

            myPopulation = elite;
            int ind = 0;
            while (myPopulation.size() < MAX_POP_SIZE){
                myPopulation.addRandom();
            }

            h = h+1;
            

        }

        return result_of_ge;

    }

    // Crossover individuals
    private static Individual crossover(Individual indiv1, Individual indiv2) {
        Individual newIndiv = new Individual(NUM_COLORS, indiv1.size());
        
        for (int i = 0; i < indiv1.size(); i++) {
            // Crossover
            if (Math.random() <= CROSSOVER_PROBABILITY) {
                newIndiv.setGene(i, indiv1.getGene(i));
            } else {
                newIndiv.setGene(i, indiv2.getGene(i));
            }
        }

        return newIndiv;
    }

    // Mutate an individual
    private static void mutate(Individual indiv) {
        // Loop through genes
        int i = (int) Math.random()*(indiv.size());
        byte gene = (byte) (Math.random()*(NUM_COLORS));
        indiv.setGene(i, gene);
    }

    // permute an individual
    private static void permute(Individual indiv) {
        for (int i = 0; i < indiv.size(); ++i){
            if (Math.random() <= PERMUTATION_PROBABILITY){
                int index_a = (int) (Math.random()*indiv.size());
                int index_b = (int) (Math.random()*indiv.size());

                byte color_a_aux = indiv.getGene(index_a);

                indiv.setGene(index_a, indiv.getGene(index_b));
                indiv.setGene(index_b, color_a_aux);
            }
        }
    }
    

    public static void noCandidate(Individual indiv) {
        for (int i = 0; i < indiv.size(); ++i){
            int index_a = (int) (Math.random()*indiv.size());
            int index_b = (int) (Math.random()*indiv.size());

            byte color_a_aux = indiv.getGene(index_a);

            indiv.setGene(index_a, indiv.getGene(index_b));
            indiv.setGene(index_b, color_a_aux);
        }
    }

    /* Getters and Setters*/
    
    public static void setNumHoles(int nH){
        NUM_HOLES = nH;
    }
    
    public static int getNumHoles(){
        return NUM_HOLES;
    }
    
    public static void setNumColors(int nC){
        NUM_COLORS = nC;
    }
    
    public static int getNumColors(){
        return NUM_COLORS;
    } 
    
}
