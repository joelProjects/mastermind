package test.testGA;

/**
 * 
 * @author Joan Llop
 */

import java.util.*;

public class DriverGA {

	private static Scanner reader = new Scanner(System.in);

	public static void main (String[] args){
		usage();
		boolean a = true;
		int op = 8;
		while (a) {
			try {
				System.out.println("\nIntroduiu opcio: ");
				op = reader.nextInt();
				a = false;
			}
			catch (Exception e){
				System.out.println("Exception: " + e.getMessage() + "\n\tl'opcio ha de ser un numero");
			}
		}
		while (op != 3){
			switch(op){
				case 1:
					if (testGA()) System.out.println("==========ok==========");
					else System.out.println("==========TEST FAIL==========");
				break;
				default:
				usage();
			}
			a = true;
			while (a) {
				try {
					System.out.println("\nIntroduiu opcio: ");
					op = reader.nextInt();
					a = false;
				}
				catch (Exception e){
					System.out.println("Exception: " + e.getMessage() + "\n\tl'opcio ha de ser un numero");
				}
			}
		}
	}

	private static boolean testGA () {
		boolean r = true;		
		try {
			String Pre = "Pre: Dificultat 0 es 3 forats i 5 colors [0..4]\n";
			Pre += "Pre: Dificultat 1 es 4 forats i 6 colors [0..5]\n";
			Pre += "Pre: Dificultat 2 es 5 forats i 8 colors [0..7]\n";
			System.out.println("Provant GA...\n" + Pre);
			System.out.println(">>>Introduiu la dificultat: ");
			int D = reader.nextInt();
			System.out.println(">>>Introduiu el codi (solucio): ");
			String Sol = reader.next();
			System.out.println("------------------------------------");
			GA gen = new GA(D);
			gen.setSolution(Sol);
			String Cand = gen.getNextCandidate();
			System.out.println(Cand);
			while (!Cand.equals(Sol)) {
				Cand = gen.getNextCandidate();
				System.out.println(Cand);
			}
			System.out.print("------------------------------------");
		}
		catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
			r = false;
		}
		return r;
	}

	private static void usage (){
		System.out.println("\nUsage:\n1 - test Genetic Algorithm\n2 - usage\n3 - exit\n");
	}
}