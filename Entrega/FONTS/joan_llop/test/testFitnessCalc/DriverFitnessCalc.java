package test.testFitnessCalc;

/**
 * 
 * @author Joan Llop
 */

import java.util.*;

public class DriverFitnessCalc {

	private static Scanner reader = new Scanner(System.in);

	public static void main (String[] args){
		usage();
		boolean a = true;
		int op = 5;
		while (a) {
			try {
				System.out.println("\nIntroduiu opcio: ");
				op = reader.nextInt();
				a = false;
			}
			catch (Exception e){
				System.out.println("Exception: " + e.getMessage() + "\n\tl'opcio ha de ser un numero");
			}
		}
		while (op != 6){
			switch(op){
				case 1:
					if (testAddPreviousCandidate()) System.out.println("==========ok==========");
					else System.out.println("==========TEST FAIL==========");
				break;
				case 2:
					if (testCheck_diffs()) System.out.println("==========ok==========");
					else System.out.println("==========TEST FAIL==========");
				break;
				case 3:
					if (testIs_Indiv_in_previous_guesses()) System.out.println("==========ok==========");
					else System.out.println("==========TEST FAIL==========");
				break;
				case 4:
					if (testGetFitness()) System.out.println("==========ok==========");
					else System.out.println("==========TEST FAIL==========");
				break;
				default:
				usage();
			}
			a = true;
			while (a) {
				try {
					System.out.println("\nIntroduiu opcio: ");
					op = reader.nextInt();
					a = false;
				}
				catch (Exception e){
					System.out.println("Exception: " + e.getMessage() + "\n\tl'opcio ha de ser un numero");
				}
			}
		}
	}

	private static boolean testAddPreviousCandidate () {
		boolean r = true;		
		try {
			String Pre = "Pre: llargada del codi < 10\n";
			Pre += "\tnumero de colors < 10\n";
			System.out.println("Provant el afegir un candidat anterior...\n" + Pre);
			System.out.println(">>>Introduiu el numero de colors: ");
			int nC = reader.nextInt();
			System.out.println(">>>Introduiu el codi del candidat: ");
			String Codi = reader.next();
			Individual indiv= new Individual(Codi, nC);
			System.out.println(">>>Introduiu les negres del codi(numero de colors iguals que la solucio): ");
			int N = reader.nextInt();
			System.out.println(">>>Introduiu les blanques del codi(numero de colors en posicio incorrecte respecte la solucio): ");
			int B = reader.nextInt();
			FitnessCalc.addPreviousCandidate(indiv, N, B);
			System.out.println("Individu afegit: " + FitnessCalc.getCandidates());
		}
		catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
			r = false;
		}
		return r;
	}

	private static boolean testCheck_diffs (){
		boolean r = true;		
		try {
			String Pre = "Pre: llargada del codi < 10\n";
			Pre += "\tnumero de colors < 10\n";
			System.out.println("Provant Check_diffs...\n" + Pre);
			System.out.println(">>>Introduiu el numero de colors del primer individu: ");
			int nC1 = reader.nextInt();
			System.out.println(">>>Introduiu el codi del candidat: ");
			String Codi = reader.next();
			Individual indiv1= new Individual(Codi, nC1);

			System.out.println(">>>Introduiu el numero de colors del segon individu: ");
			int nC2 = reader.nextInt();
			System.out.println(">>>Introduiu el codi del candidat: ");
			String Codi2 = reader.next();
			Individual indiv2= new Individual(Codi2, nC2);

			int diffs = FitnessCalc.check_diffs(indiv1, indiv2);
			System.out.println("N: " + diffs/10 + " B: " + diffs%10);
		}
		catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
			r = false;
		}
		return r;
	}

	private static boolean testIs_Indiv_in_previous_guesses (){
		boolean r = true;		
		try {
			String Pre = "Pre: llargada del codi < 10\n";
			Pre += "\tnumero de colors < 10\n";
			System.out.println("Provant Is_Indiv_in_previous_guesses...\n" + Pre);

			System.out.println(">>>Introduiu el numero de colors d'un individu anterior: ");
			int nC = reader.nextInt();
			System.out.println(">>>Introduiu el codi del candidat: ");
			String Codi = reader.next();
			Individual indiv= new Individual(Codi, nC);

			FitnessCalc.addPreviousCandidate(indiv, 0, 0);

			System.out.println(">>>Introduiu el numero de colors d'un individu anterior: ");
			int nC1 = reader.nextInt();
			System.out.println(">>>Introduiu el codi del candidat: ");
			String Codi1 = reader.next();
			Individual indiv1= new Individual(Codi1, nC1);

			System.out.println("Individu ja triat: " + FitnessCalc.is_Indiv_in_previous_guesses(indiv1));
		}
		catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
			r = false;
		}
		return r;
	}

	private static boolean testGetFitness(){

		boolean r = true;		
		try {
			String Pre = "Pre: llargada del codi < 10\n";
			Pre += "\tnumero de colors < 10\n";
			System.out.println("Provant el afegir un getFitness...\n" + Pre);
			System.out.println(">>>Introduiu el numero de colors: ");
			int nC = reader.nextInt();
			System.out.println(">>>Introduiu el codi del candidat: ");
			String Codi = reader.next();
			Individual indiv= new Individual(Codi, nC);

			System.out.println("Fitness de l'individu: " + FitnessCalc.getFitness(indiv));
		}
		catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
			r = false;
		}
		return r;
	}

	private static void usage (){
		System.out.println("\nUsage:\n1 - test addPreviousCandidate\n2 - test check_diffs\n3 - test is_Indiv_in_previous_guesses\n4 - test getFitness\n5 - usage\n6 - exit\n");
	}
}