package domini;

/**
 * 
 * @author Joan Llop
 */

public class Individual implements Comparable<Individual>{

    private int NUM_COLORS;

    private int defaultGeneLength;

    private byte[] genes;

    private int fitness = -1;

    public void generateIndividual() {
        for (int i = 0; i < defaultGeneLength; i++) {
            byte gene = (byte) (Math.random()*(NUM_COLORS));
            genes[i] = gene;
        }
    }
    
    /**
    * Pre: llargada del codi < 10
    * Pre: numeros de colors < 10
    * 
    */
    public Individual (int nC, int nH){
        setDefaultGeneLength(nH);
        setNumColors(nC);
        genes = new byte[defaultGeneLength];
    }

    /**
    * Pre: el codi nomes contene numeros(colors) entre 0 i numero de colors
    * Pre: llargada del codi < 10
    * Pre: numeros de colors < 10
    * 
    */
    public Individual (byte[] c, int nC){
        setNumColors(nC);
        setDefaultGeneLength(c.length);
        setGenes(c);
    }

    /**
    * Pre: el codi nomes contene numeros(colors) entre 0 i numero de colors
    * Pre: llargada del codi < 10
    * Pre: numeros de colors < 10
    */
    public Individual (String s, int nC){
        setNumColors(nC);
        setDefaultGeneLength(s.length());
        for (int i = 0; i < s.length(); ++i) {
            setGene(i, (byte) (s.charAt(i) - '0'));
        }
    }

    /* Getters and Setters */

    public void setNumColors (int num_colors){
        NUM_COLORS = num_colors;
    }

    public void setDefaultGeneLength(int length) {
        defaultGeneLength = length;
        genes = new byte[defaultGeneLength];
    }

    /**
    * Pre: valors entre 0 i numero de colors
    * Pre: size == defaultGeneLength
    * 
    */
    public void setGenes(byte[] G){
        for (int i = 0; i < G.length; ++i){
            setGene(i, G[i]);
        }
    }

    /**
    * Pre: valor entre 0 i numero de colors
    * Pre: index entre 0 i defaultGeneLength
    * 
    */
    public void setGene(int index, byte value) {
        genes[index] = value;
        fitness = -1;
    }

    public int getNumColors(){
        return NUM_COLORS;
    }
    
    public byte getGene(int index) {
        return genes[index];
    }

    public byte[] getGenes (){
        byte[] r = new byte[genes.length];
        for (int i = 0; i < r.length; ++i){
            r[i] = getGene(i);
        }
        return r;
    }
    
    public int getFitness() {
        if (fitness == -1) {
            fitness = FitnessCalc.getFitness(this);
        }
        return fitness;
    }
    
    

    /* Public methods */
    public int size() {
        return genes.length;
    }

    public boolean equals(Individual Indiv){
        if (Indiv.size() != defaultGeneLength) return false;
        for (int i = 0; i < defaultGeneLength; ++i){
            if (Indiv.getGene(i) != genes[i]) return false;
        }
        return true;
    }

    public Individual clone (){
        Individual clone_indiv = new Individual(NUM_COLORS, defaultGeneLength);
        clone_indiv.setGenes(genes);
        return clone_indiv;
    }

    @Override
    public int compareTo(Individual compareIndiv) {
        int fitness_score_param = compareIndiv.getFitness();
        int fitness_score_this = this.getFitness();
        if (fitness_score_this < fitness_score_param){
            return -1;
        }
        else if (fitness_score_this > fitness_score_param){
            return 1;
        }
        else return 0;
    }

    @Override
    public String toString() {
        String geneString = "";
        for (int i = 0; i < size(); i++) {
            geneString += getGene(i);
        }
        return geneString;
    }
}
