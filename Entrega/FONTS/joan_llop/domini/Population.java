package domini;

import java.util.*;

/**
 * 
 * @author Joan Llop
 */

public class Population {
    
    private int NUM_COLORS;
    private int NUM_HOLES;

    ArrayList<Individual> individuals = new ArrayList<Individual>();

    /**
    * Pre: llargada dels codis < 10
    * Pre: numero de colors < 10
    */
    public Population(int populationSize, int nC, int nH) {
        NUM_COLORS = nC;
        NUM_HOLES = nH;
        for (int i = 0; i < populationSize; i++) {
            this.addRandom();
        }
    }

    /* Getters and Setters*/
    
    /**
    * Pre: index entre 0 i individuals.size()
    */
    public Individual getIndividual(int index) {
        return individuals.get(index);
    }

    public Individual getFittest() {
        Individual fittest = getIndividual(0);
        // Loop through individuals to find fittest
        for (int i = 0; i < size(); i++) {
            if (fittest.getFitness() < getIndividual(i).getFitness()) {
                fittest = getIndividual(i);
            }
        }
        return fittest;
    }
    
    /**
    * Pre: numero de colors < 10
    */
    public void setNumColors(int nC){
        NUM_COLORS = nC;
    }
    
    public int getNumColors(){
        return NUM_COLORS;
    }
    
    /**
    * Pre: llargada dels codis < 10
    */
    public void setNumHoles(int nH){
        NUM_HOLES = nH;
    }
    
    public int getNumHoles(){
        return NUM_HOLES;
    }


    /* Public methods */
    public int size() {
        return individuals.size();
    }

    /**
    * Pre: index entre 0 i individuals.size()
    */
    public void saveIndividual(int index, Individual indiv) {
        individuals.set(index, indiv);
    }

    /**
    * Pre: index entre 0 i individuals.size()
    */
    public void removeIndividual (int index){
        individuals.remove(index);
    }

    public void addIndividual (Individual indiv){
        individuals.add(indiv);
    }

    public void addRandom (){
        Individual randomIndiv = new Individual(NUM_COLORS, NUM_HOLES);
        randomIndiv.generateIndividual();
        this.addIndividual(randomIndiv);
    }

    public void sort(){
        Collections.sort(individuals);
    }
}
