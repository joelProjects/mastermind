package mastermind.domini;

/**
 * 
 * @author Joel Moreno
 */


public class ControladorDomini {
    
    private Partida partidaActual; 
    public Ranking ranking;

    public ControladorDomini(){ //inicialitzacio del controlador
        //TODO carregarRanking de fitxer
        ranking = new Ranking();; // aixo no sera aixi, s'ha de carregar el ranking del fitxer
        partidaActual = null;
    } 

    public void crearPartida(String nom, int dificultat, String mode, int rol) throws Exception{ //crea una partida amb la ronda inicial 
        if(dificultat < 0 || dificultat > 2) throw new Exception("La dificultat ha d'estar entre 0 i 2");
        if(rol != 0 && rol != 1) throw new Exception("El rol ha de ser 0(codemaker) o 1(codebreaker)");
        if(mode.compareTo("practica") != 0 && mode.compareTo("competitiu") != 0) throw new Exception("El mode ha de ser practica o competitiu");
        partidaActual = new Partida(mode, dificultat, rol, nom);
        
        if(partidaActual.getRolInicial() == 1){
            partidaActual.novarondacodebreaker();
        }
    }

    public String creaRondaCodemaker(String solucio) throws Exception{
        int dif = partidaActual.getdificultat();
        if(dif == 0){
            if(solucio.length() != 3) throw new Exception("Llargada no valida");
            for(int i = 0; i < 3; ++i) if(solucio.charAt(i) > '4' || solucio.charAt(i) < '0') throw new Exception("Color no valid");
        }
        if(dif == 1){
            if(solucio.length() != 4) throw new Exception("Llargada no valida");
            for(int i = 0; i < 4; ++i) if(solucio.charAt(i) > '5' || solucio.charAt(i) < '0') throw new Exception("Color no valid");
        }
        if(dif == 2){
            if(solucio.length() != 5) throw new Exception("Llargada no valida");
            for(int i = 0; i < 5; ++i) if(solucio.charAt(i) > '7' || solucio.charAt(i) < '0') throw new Exception("Color no valid");
        }
        String candidat = partidaActual.novarondacodemaker(solucio);
        return candidat;
    }

    public String jugar(String input) throws Exception{
        if(partidaActual.getRolActual() == 0) return jugarCodemaker(input);
        else return jugarCodebreaker(input);
    }

    private String jugarCodebreaker(String candidat) throws Exception{  //falten coses com quan s'encerta la solucio
        int dif = partidaActual.getdificultat();
        if(dif == 0){
            if(candidat.length() != 3) throw new Exception("Llargada no valida");
            for(int i = 0; i < 3; ++i) if(candidat.charAt(i) > '4' || candidat.charAt(i) < '0') throw new Exception("Color no valid");
        }
        if(dif == 1){
            if(candidat.length() != 4) throw new Exception("Llargada no valida");
            for(int i = 0; i < 4; ++i) if(candidat.charAt(i) > '5' || candidat.charAt(i) < '0') throw new Exception("Color no valid");
        }
        if(dif == 2){
            if(candidat.length() != 5) throw new Exception("Llargada no valida");
            for(int i = 0; i < 5; ++i) if(candidat.charAt(i) > '7' || candidat.charAt(i) < '0') throw new Exception("Color no valid");
        }
        return partidaActual.novajugadacodebreaker(candidat);
    }
    
    private String jugarCodemaker(String bn)throws Exception{ //falten coses com quan s'encerta la solucio
        int dif = partidaActual.getdificultat();
        if(dif == 0){
            if(bn.length() > 3) throw new Exception("Llargada no valida");
        }
        if(dif == 1){
            if(bn.length() > 4) throw new Exception("Llargada no valida");
        }
        if(dif == 2){
            if(bn.length() > 5) throw new Exception("Llargada no valida");
        }
        for(int i = 0; i < bn.length(); ++i) if(bn.charAt(i) != 'B' && bn.charAt(i) != 'N') throw new Exception("Nomes B o N");
        return partidaActual.novajugadacodemaker(bn);
    }

    public void finalitzarRonda(){ //genera la ronda seguent o va a finalitza partida 
        if(partidaActual.getmode().compareTo("practica") == 0) finalitzarPartida();
        else{
            if(partidaActual.getNrondes() < 4 && partidaActual.getRolActual() == 0){
                partidaActual.novarondacodebreaker();
            } 
            else if(partidaActual.getNrondes() == 4) finalitzarPartida();
        }
    }
    
    private void finalitzarPartida(){ //si era competicio s'intenta collocar la puntuacio al ranking
    //la puntuació encara no está implementada i el ranking no forma part de la jugabilitat 
    //aixi que estará comentat

    if(partidaActual.getmode().compareTo("competitiu") == 0){
        int puntuacio = partidaActual.calcularPuntuacio();
        String nom = partidaActual.getnom();
        if(partidaActual.getdificultat() == 0) ranking.setFacil(puntuacio, nom);
        else if(partidaActual.getdificultat() == 1) ranking.setMitjana(puntuacio, nom);
        else ranking.setDificil(puntuacio, nom);
    }
    partidaActual = null;
    //TODO esborrar partida
}

    public void guardarPartida(){  
        
    }

    public void carregarPartida(){  
        
    }

    public void esborrarPartida(){ 
        
    }

}