package mastermind.domini;

/**
 * 
 * @author Joel Moreno
 */


import java.util.ArrayList;

public class Ranking {
    
    private ArrayList<Posicion> facil;
    private ArrayList<Posicion> mitjana;
    private ArrayList<Posicion> dificil;

    public Ranking(){
        facil = new ArrayList<Posicion>();
        mitjana = new ArrayList<Posicion>();
        dificil = new ArrayList<Posicion>();
    } 

    public ArrayList<Posicion> getFacil(){
        return facil;
    }

    public ArrayList<Posicion> getMitjana(){
        return mitjana;
    }

    public ArrayList<Posicion> getDificil(){
        return dificil;
    }

    public void setFacil(int score, String name){
        int pos = -1;
        for(int i = 0; i < facil.size() && pos == -1; ++i){
            if(facil.get(i).puntuacion < score) pos = i;
        }
        if(pos > -1){
            facil.add(pos, new Posicion(name, score));
            if(facil.size() > 10) facil.remove(10);
        }
    }

    public void setMitjana(int score, String name){
        int pos = -1;
        for(int i = 0; i < mitjana.size() && pos == -1; ++i){
            if(mitjana.get(i).puntuacion < score) pos = i;
        }
        if(pos > -1){
            mitjana.add(pos, new Posicion(name, score));
            if(mitjana.size() > 10) mitjana.remove(10);
        }
    }

    public void setDificil(int score, String name){
        int pos = -1;
        for(int i = 0; i < dificil.size() && pos == -1; ++i){
            if(dificil.get(i).puntuacion < score) pos = i;
        }
        if(pos > -1){
            dificil.add(pos, new Posicion(name, score));
            if(dificil.size() > 10) dificil.remove(10);
        }
    }



}