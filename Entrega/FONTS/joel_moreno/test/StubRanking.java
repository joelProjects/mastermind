package tests;
/**
 * 
 * @author Joel Moreno
 */


public class StubRanking {

    public StubRanking(){ //inicialitzacio del controlador
    } 

    public void setFacil(int puntuacio, String nom){
        System.out.println("Set puntuacio: " + puntuacio);
        System.out.println("amb nom: " + nom);
        System.out.println("en ranking facil");
    }

    public void setMitjana(int puntuacio, String nom){
        System.out.println("Set puntuacio: " + puntuacio);
        System.out.println("amb nom: " + nom);
        System.out.println("en ranking mitja");
    }

    public void setDificil(int puntuacio, String nom){
        System.out.println("Set puntuacio: " + puntuacio);
        System.out.println("amb nom: " + nom);
        System.out.println("en ranking dificil");
    }

    
}