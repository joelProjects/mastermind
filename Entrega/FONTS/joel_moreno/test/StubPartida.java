package tests;
/**
 * 
 * @author Joel Moreno
 */


public class StubPartida {

    private int rolInicial;
    private int dificultat;
    private String mode;
    private String nom;
    private int rondes;

    public StubPartida(String mod, int diff, int role, String name){ //inicialitzacio del controlador
        System.out.println("Es crea una partida amb: ");
        nom = name;
        System.out.println("nom: " + name);
        dificultat = diff;
        System.out.println("dificultat: " + diff);
        rolInicial = role;
        System.out.println("rol: " + role);
        mode = mod;
        System.out.println("mode: " + mod);
        rondes = 0;
    } 

    public int getRolInicial(){
        return rolInicial;
    }

    public int getdificultat(){
        return dificultat;
    }

    public String getmode(){
        return mode;
    }

    public String getnom(){
        return nom;
    }

    public int calcularPuntuacio(){
        return 42;
    }

    public int getNrondes(){
        return rondes;
    }

    public int getRolActual(){
        return (rondes + 1 + rolInicial) % 2;
    }

    public void novarondacodebreaker(){
        System.out.println("Es crea una nova ronda codebreaker");
        ++rondes;
    }

    public String novarondacodemaker(String solucio){
        ++rondes;
        System.out.println("Es crea una nova ronda codemaker amb solucio: " + solucio);
        return "candidat";
    }

    public String novajugadacodebreaker(String candidat){
        System.out.println("Es crea una nova jugada codebreaker");
        System.out.println("Candidat: " + candidat);
        return "BN";
    }

    public String novajugadacodemaker(String bn){
        System.out.println("Es crea una nova jugada codemaker");
        System.out.println("BN: " + bn);
        return "candidat";
    }
}