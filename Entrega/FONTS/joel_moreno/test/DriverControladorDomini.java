package tests;
import java.util.*;
import java.io.File;
import java.io.FileNotFoundException;
/**
 * 
 * @author Joel Moreno
 */


public class DriverControladorDomini {

    Scanner SIO;
	boolean interactive;

    public static void main (String[] args) throws Exception{
        int tests = 1;
		Scanner S;
		boolean inter;
		if(args.length>0){
			File file = new File(args[0]);
			S=new Scanner(file);
			inter=false;
		}
		else{
			S=new Scanner(System.in);
			inter=true;
		}		
		DriverControladorDomini d = new DriverControladorDomini(S,inter);
		if(inter) System.out.println("\nRecorda que pots pasar un fitxer com a parametre per ser llegit com a entrada");
		if(inter) d.usage();
		String op = S.next();
		while (!op.equals("exit")){
            try{
                System.out.println("Test " + tests + ": \n");
                switch(op){
                    case "testConstructora":					
                        d.testConstructora();
                    break;
                    case "testCrearPartida":
                        d.testCrearPartida();
                    break;
                    case "testCreaRondaCodemaker":
                        d.testCreaRondaCodemaker();
                    break;
                    case "testJugadaCodebreaker":
                        d.testJugadaCodebreaker();
                    break;
                    case "testJugadaCodemaker":
                        d.testJugadaCodemaker();
                    break;
                    case "testFinalitzarRonda":
                        d.testFinalitzarRonda();
                    break;
                    case "testFinalitzarPartida":
                        d.testFinalitzarPartida();
                    break;
                    default:
                        d.usage();
                    break;
                }
            }
            catch(Exception e){
                System.out.println(e.getMessage());
            }
            System.out.println("\n");
            ++tests;
			op = S.next();
		}
    }
    
    public DriverControladorDomini(Scanner S,boolean inter){
		SIO=S;
        interactive=inter;
	}

    public void testConstructora(){ 
        ControladorDomini control = new ControladorDomini(); 
        StubPartida par = control.getPartidaActual(); 
        System.out.println("Partida: " + par);
        System.out.println("Ranking: " + control.ranking);
    } 

    public void testCrearPartida() throws Exception{
        if(interactive) System.out.println("Introdueix el nom: ");
		String nom = SIO.next();
		if(interactive) System.out.println("Introdueix la dificultat: ");
        String dificultat = SIO.next();
        int dif = dificultat.charAt(0) - '0';
        if(interactive) System.out.println("Introdueix el mode: ");
		String mode = SIO.next();
		if(interactive) System.out.println("Introdueix el rol: ");
        String roleplay = SIO.next();
        int rol = roleplay.charAt(0) - '0';
        ControladorDomini control = new ControladorDomini(); 
        control.crearPartida(nom, dif, mode, rol);

        System.out.println("Partida dins de control deixa de ser null");
        StubPartida par = control.getPartidaActual(); 
        System.out.println("Partida: " + par);
    }

    public void testCreaRondaCodemaker() throws Exception{
        ControladorDomini control = new ControladorDomini();
        System.out.println("Es creara una partida amb nom test i rol codemaker");
        System.out.println("Com el mode es trivial li assignarem practica");
        if(interactive) System.out.println("Introdueix la dificultat: ");
        String dificultat = SIO.next();
        int dif = dificultat.charAt(0) - '0';
        control.crearPartida("test", dif, "practica", 0);
        if(interactive) System.out.println("Introdueix la solucio: ");
        String solucio = SIO.next();
        String candidat = control.creaRondaCodemaker(solucio);
        System.out.println("return in candidat: " + candidat);
    }

    //Com jugar() l'unic que fa es redireccionar a codebreaker o a codemaker
    //el testejem directament dins de testCodebreaker i testCodemaker
    public void testJugadaCodebreaker() throws Exception{ 
        System.out.println("Es creara una partida amb nom test i rol codebreaker");
        System.out.println("El mode es trivial en aquest test aixi que sera practica");
        
        if(interactive) System.out.println("Introdueix la dificultat: ");
        String dificultat = SIO.next();
        int dif = dificultat.charAt(0) - '0';
        
        ControladorDomini control = new ControladorDomini();
        control.crearPartida("test", dif, "practica", 1);
        
        if(interactive) System.out.println("Introdueix el candidat: ");
        String candidat = SIO.next();

        String out = control.jugar(candidat);
        System.out.println("Output: " + out);
    }
    
    public void testJugadaCodemaker()throws Exception{ 
        System.out.println("Es creara una partida amb nom test i rol codemaker");
        System.out.println("El mode es trivial en aquest test aixi que sera competitiu");
        if(interactive) System.out.println("Introdueix la dificultat: ");
        String dificultat = SIO.next();
        int dif = dificultat.charAt(0) - '0';

        ControladorDomini control = new ControladorDomini();
        control.crearPartida("test", dif, "competitiu", 0);

        if(interactive) System.out.println("Introdueix la solucio: ");
        String solucio = SIO.next();

        String candidat = control.creaRondaCodemaker(solucio);
        System.out.println("return in candidat: " + candidat);

        if(interactive) System.out.println("Introdueix les blanques i negres (NNNBB): ");
        String bn = SIO.next();

        String out = control.jugar(bn);
        System.out.println("Output: " + out);
    }

    //Finalitza una ronda precedida per una de nova
    public void testFinalitzarRonda() throws Exception{ 
        System.out.println("Es creara una partida amb nom test i mode competitiu");
        System.out.println("La dificultat es trivial en aquest test aixi que li donarem el valor dificil");
        if(interactive) System.out.println("Introdueix el rol: ");
        String roleplay = SIO.next();
        int rol = roleplay.charAt(0) - '0';
        
        ControladorDomini control = new ControladorDomini();
        control.crearPartida("test", 2, "competitiu", rol);
        if(rol == 0) control.creaRondaCodemaker("12345"); //la solucio en aquest test es trivial
        System.out.println("NRondesActual: " + control.numRondes());
        control.finalitzarRonda();

        if(rol == 1){
            System.out.println("Despres d'una ronda codebreaker s'ha d'iniciar la ronda codemaker manualment ja que necessitem la solucio");
            System.out.println("NRondesActual: " + control.numRondes());
        }
        else{
            System.out.println("Despres d'una ronda codemaker s'inicia la seguent ronda codebreaker automaticament");
            System.out.println("NRondesActual: " + control.numRondes());
        }

    }
    
    //Finalitza una ronda final que dona lloc al final de la partida
    public void testFinalitzarPartida() throws Exception{ 
        System.out.println("Es creara una partida amb nom test i mode practica");
        System.out.println("La resta de valors no son importants aixi que els hi donarem els valors facil i codebreaker");
    
        ControladorDomini control = new ControladorDomini();
        control.crearPartida("test", 0, "practica", 1);
        StubPartida actual = control.getPartidaActual();
        if(actual != null) System.out.println("partidaActual no es null, correcte");
        else System.out.println("partidaActual es null, incorrecte");

        System.out.println("Ara es finalitzarà la ronda i com a consequencia la partida ja que es mode practica");
        control.finalitzarRonda();
        actual = control.getPartidaActual();
        if(actual != null) System.out.println("partidaActual no es null, incorrecte");
        else System.out.println("partidaActual es null, correcte");
    }

    public void usage(){
        System.out.println("\nusage: ");
		System.out.println("  testConstructora");
		System.out.println("  testCrearPartida");
        System.out.println("  testCreaRondaCodemaker");
        System.out.println("  testJugadaCodebreaker");
        System.out.println("  testJugadaCodemaker");
        System.out.println("  testFinalitzarRonda");
        System.out.println("  testFinalitzarPartida");
		System.out.println("  exit");
		System.out.println("  usage\n");
    }
}