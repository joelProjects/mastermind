package mastermind.domini;

import java.util.ArrayList;
import java.util.Random;
        
/**
 *
 * @author Oliver   5 3 6 4 8 5
 */
public class Ronda {
    protected Individual solucio;
    protected ArrayList<Jugada> jugades;
    protected int dificultat; //0 1 2

    public Ronda () {
        jugades= new ArrayList<Jugada>();
    }
    
    public Ronda(int dif){
        //TODO
        //generar la solucion random
        Random rn = new Random();
        switch (dif) {
            case 0:
                solucio = new Individual("" + rn.nextInt(5) + rn.nextInt(5) + rn.nextInt(5), 5);
                break;
            case 1:
                solucio = new Individual("" + rn.nextInt(6) + rn.nextInt(6) + rn.nextInt(6) + rn.nextInt(6), 6);
                break;
            case 2:
                solucio = new Individual("" + rn.nextInt(8) + rn.nextInt(8) + rn.nextInt(8) + rn.nextInt(8) + rn.nextInt(8), 8);
                break;
            default:
                break;
        }
        jugades= new ArrayList<Jugada>();
        dificultat=dif;
    }
    
    
    public void afegirJugada(String candidat) throws Exception{
        if(jugades.size() >= 12) throw new Exception("Se ha llegado al máximo de jugadas");
        Jugada new_jugada = new Jugada(candidat,solucio.toString());
        jugades.add(new_jugada);
    }
    
    public String getSolucio(){
        return solucio.toString();
    }
    
    public int getNjugades(){
        return jugades.size();
    }
    
    public int getDificultat(){
        return dificultat;
    }
    
    public Jugada getjugada(int i){
        if(i>=0 && i<jugades.size()) return jugades.get(i);
        throw new IndexOutOfBoundsException() ;
    }
    
    
    
    
    
}
