package mastermind.domini;

import java.util.ArrayList;

/**
 *
 * @author Oliver
 */
public class Partida {
    private final String mode; //competitiu | practica
    private final int dificultat; //0 1 2
    private ArrayList<Ronda> rondes;
    private final int rolInicial; //0 CM 1 CB
    private final String nom;
    
    public Partida(String m, int dif, int rol, String n){
        mode=m;
        dificultat=dif;
        rolInicial=rol;
        rondes= new ArrayList<>();
        nom=n;
    }
    
    public String novarondacodemaker(String solucion) throws Exception{
        GA new_ronda = new GA(dificultat);
        new_ronda.setSolution(solucion);
        new_ronda.getNextCandidate(); 
        rondes.add(new_ronda);   
        return new_ronda.getjugada(new_ronda.getNjugades()-1).getCandidat();
    }

    public void novarondacodebreaker(){
        Ronda new_ronda = new Ronda(dificultat);
        rondes.add(new_ronda); 
    }
    
    public String novajugadacodebreaker(String candidat) throws Exception{
        Ronda ronda_actual = rondes.get(rondes.size() - 1);
        ronda_actual.afegirJugada(candidat);
        return ronda_actual.getjugada(ronda_actual.getNjugades() - 1).getBN();
    }
    
    public String novajugadacodemaker(String BN) throws Exception{
        GA ronda_actual = (GA) rondes.get(rondes.size() - 1);
		int N = 0;
        int B = 0;
        for (int i = 0; i < BN.length(); ++i) {
            if (BN.charAt(i) == 'N') ++N;
            else ++B;
        }
		String NB= ronda_actual.getjugada(ronda_actual.getNjugades() - 1).getBN();
        for (int i = 0; i < NB.length(); ++i) {
            if (NB.charAt(i) == 'N') --N;
            else --B;
        }
        if(N!=0 || B!=0) throw new Exception("El jugador no esta introduciendo las BN correctamente");
        ronda_actual.getNextCandidate();
        return ronda_actual.getjugada(ronda_actual.getNjugades()-1).getCandidat();
    }
    
    public Ronda getRonda(int i){
        if(i>=0 && i<rondes.size()) return rondes.get(i);
        throw new IndexOutOfBoundsException() ;
    }
    
    public int calcularPuntuacio(){
        if(mode.equals("practica")) return 0;
        return 42;
        /*
           TODO 
        */
    }
    
    public int getNrondes(){
        return rondes.size();
    }
    
    public String getmode(){
        return mode;
    }
    
    public int getdificultat(){
        return dificultat;
    }
    
    public int getRolActual(){
		if(rondes.size()==0) throw new RuntimeException();
        return (rondes.size()+1 + rolInicial) % 2;
    }

    public int getRolInicial(){
        return rolInicial;
    }
    
    public String getnom(){
        return nom;
    }
}
