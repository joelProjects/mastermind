package test.domini;

/**
 * 
 * @author Joan Llop
 */

import java.util.*;

public class DriverIndividual {
	public static void main (String[] args){
		usage();
		boolean a = true;
		int op = 8;
		while (a) {
			try {
				op = Integer.parseInt(System.console().readLine("\nIntroduiu opcio: "));
				a = false;
			}
			catch (Exception e){
				System.out.println("Exception: " + e.getMessage() + "\n\tl'opcio ha de ser un numero");
			}
		}
		while (op != 9){
			switch(op){
				case 1:
					if (testCreatorWithString()) System.out.println("==========ok==========");
					else System.out.println("==========TEST FAIL==========");
				break;
				case 2:
					if (testCreatorWithBytes()) System.out.println("==========ok==========");
					else System.out.println("==========TEST FAIL==========");
				break;
				case 3:
					if (testEquals()) System.out.println("==========ok==========");
					else System.out.println("==========TEST FAIL==========");
				break;
				case 4:
					if (testGenerate()) System.out.println("==========ok==========");
					else System.out.println("==========TEST FAIL==========");
				break;
				case 5:
					if (testClone()) System.out.println("==========ok==========");
					else System.out.println("==========TEST FAIL==========");
				break;
				case 6:
					if (testCompareTo()) System.out.println("==========ok==========");
					else System.out.println("==========TEST FAIL==========");
				break;
				case 7:
					if (testToString()) System.out.println("==========ok==========");
					else System.out.println("==========TEST FAIL==========");
				break;
				default:
				usage();
			}
			a = true;
			while (a) {
				try {
					op = Integer.parseInt(System.console().readLine("\nIntroduiu opcio: "));
					a = false;
				}
				catch (Exception e){
					System.out.println("Exception: " + e.getMessage() + "\n\tl'opcio ha de ser un numero");
				}
			}
		}
	}

	private static boolean testGenerate () {
		boolean r = true;		
		try {
			String Pre = "Pre: llargada del codi < 10\n";
			Pre += "\tnumero de colors < 10\n";
			System.out.println("Provant el generador aleatori...\n" + Pre);
			int nC = Integer.parseInt(System.console().readLine(">>>Introduiu el numero de colors: "));
			int nH = Integer.parseInt(System.console().readLine(">>>Introduiu la llargada del codi a generar: "));
			Individual indiv= new Individual(nC, nH);
			indiv.generateIndividual();
			System.out.println("Individu generat: " + indiv.toString());
		}
		catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
			r = false;
		}
		return r;
	}

	private static boolean testCreatorWithString (){
		boolean r = true;		
		try {
			String Pre = "Pre: el codi nomes conte numeros(colors) entre 0 i numero de colors\n";
			Pre += "\tllargada del codi < 10\n";
			Pre += "\tnumero de colors < 10\n";
			System.out.println("Provant la creadora amb String...\n" + Pre);
			int nC = Integer.parseInt(System.console().readLine(">>>Introduiu el numero de colors: "));
			String Cod = System.console().readLine(">>>Introduiu el codi (Exemple: 1234): ");
			Individual indiv= new Individual(Cod, nC);
			System.out.println("Individu creat: " + indiv.toString());
		}
		catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
			r = false;
		}
		return r;
	}

	private static boolean testCreatorWithBytes (){
		boolean r = true;		
		try {
			String Pre = "Pre: el codi nomes conte numeros(colors) entre 0 i el numero de colors\n";
			Pre += "\tllargada del codi < 10\n";
			Pre += "\tnumero de colors < 10\n";
			System.out.println("Provant la creadora amb Bytes...\n" + Pre);
			int nC = Integer.parseInt(System.console().readLine(">>>Introduiu el numero de colors: "));
			String Cod = System.console().readLine(">>>Introduiu el codi (Exemple: 1234): ");
			byte[] Codi = new byte[Cod.length()];
			for (int i = 0; i < Cod.length(); ++i){
				Codi[i] = (byte) (Cod.charAt(i) - '0');
			}
			Individual indiv= new Individual(Codi, nC);
			System.out.println("Individu creat: " + indiv.toString());
		}
		catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
			r = false;
		}
		return r;
	}

	private static boolean testEquals(){

		boolean r = true;		
		try {
			String Pre = "Pre: els codis nomes contenen numeros(colors) entre 0 i numero de colors\n";
			Pre += "\tllargada dels codis < 10\n";
			Pre += "\tnumeros de colors < 10\n";
			System.out.println("Provant la funcio equals...\n" + Pre);
			int nC1 = Integer.parseInt(System.console().readLine(">>>Introduiu el numero de colors del primer individu: "));
			String Cod1 = System.console().readLine(">>>Introduiu el codi del primer individu (Exemple: 1234): ");

			int nC2 = Integer.parseInt(System.console().readLine(">>>Introduiu el numero de colors del segon individu: "));
			String Cod2 = System.console().readLine(">>>Introduiu el codi del segon individu (Exemple: 1234): ");

			Individual indiv1= new Individual(Cod1, nC1);
			Individual indiv2= new Individual(Cod2, nC2);
			System.out.println("resultat de comparar-los: " + indiv1.equals(indiv2));
		}
		catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
			r = false;
		}
		return r;
	}
	
	private static boolean testClone(){
		boolean r = true;		
		try {
			String Pre = "Pre: el codi nomes conte numeros(colors) entre 0 i el numero de colors\n";
			Pre += "\tllargada del codi < 10\n";
			Pre += "\tnumero de colors < 10\n";
			System.out.println("Provant la funcio clone...\n" + Pre);
			int nC = Integer.parseInt(System.console().readLine(">>>Introduiu el numero de colors: "));
			String Cod = System.console().readLine(">>>Introduiu el codi (Exemple: 1234): ");

			Individual indiv= new Individual(Cod, nC);
			Individual indiv_clonat = indiv.clone();

			indiv.setGene(indiv.size()-1, (byte) (-1));
			System.out.println("Individu clonat: " + indiv_clonat.toString());
		}
		catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
			r = false;
		}
		return r;
	}

	private static boolean testCompareTo(){
		boolean r = true;		
		try {
			String Pre = "Pre: els codis nomes contenen numeros(colors) entre 0 i numero de colors\n";
			Pre += "\tllargada dels codis < 10\n";
			Pre += "\tnumeros de colors < 10\n";
			System.out.println("Provant la funcio compareTo...\n" + Pre);
			int nC1 = Integer.parseInt(System.console().readLine(">>>Introduiu el numero de colors del primer individu: "));
			String Cod1 = System.console().readLine(">>>Introduiu el codi del primer individu (Exemple: 1234): ");

			int nC2 = Integer.parseInt(System.console().readLine(">>>Introduiu el numero de colors del segon individu: "));
			String Cod2 = System.console().readLine(">>>Introduiu el codi del segon individu (Exemple: 1234): ");

			Individual indiv1= new Individual(Cod1, nC1);
			Individual indiv2= new Individual(Cod2, nC2);
			System.out.println("resultat de comparar-los (hauria de donar sempre 0 perque el resultat prove del stub fitness class): " + indiv1.compareTo(indiv2));
		}
		catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
			r = false;
		}
		return r;
	}


	private static boolean testToString(){
		boolean r = true;		
		try {
			String Pre = "Pre: el codi nomes conte numeros(colors) entre 0 i el numero de colors\n";
			Pre += "\tllargada del codi < 10\n";
			Pre += "\tnumero de colors < 10\n";
			System.out.println("Provant la funcio toString...\n" + Pre);
			int nC = Integer.parseInt(System.console().readLine(">>>Introduiu el numero de colors: "));
			String Cod = System.console().readLine(">>>Introduiu el codi (Exemple: 1234): ");
			Individual indiv= new Individual(Cod, nC);
			System.out.println("Individu toString: " + indiv.toString());
		}
		catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
			r = false;
		}
		return r;
	}
 

	private static void usage (){
		System.out.println("\nUsage:\n1 - test creadora amb String\n2 - test creadora amb bytes\n3 - test equals\n4 - test generadora\n5 - test clone\n6 - test CompareTo\n7 - test ToString\n8 - usage\n9 - exit\n");
	}
}