package test.domini;
import java.util.*;
import java.io.File;
import java.io.FileNotFoundException;
/**
 *
 * @author Oliver
 */
public class DriverJugada {
	
	Scanner SIO;
	boolean interactive;
    
    public static void main (String[] args) throws Exception{
		Scanner S;
		boolean inter;
		if(args.length>0){
			File file = new File(args[0]);
			S=new Scanner(file);
			inter=false;
		}
		else{
			S=new Scanner(System.in);
			inter=true;
		}		
		DriverJugada d = new DriverJugada(S,inter);
		if(inter) System.out.println("\nRecorda que pots pasar un fitxer com a parametre per ser llegit com a entrada");
		if(inter) d.usage();
		String op = S.next();
		while (!op.equals("exit")){
			switch(op){
				case "testjugada":					
					d.testjugada();
				break;
				case "testgetCandidat":
					d.testgetCandidat();
				break;
				case "testgetBN":
					d.testgetBN();
				break;
				default:
					d.usage();
				break;
			}
			op = S.next();
		}
	}
	
	public DriverJugada(Scanner S,boolean inter){
		SIO=S;
		interactive=inter;
	}
   
    public void testjugada(){
		if(interactive) System.out.println("Introdueix el candidat: ");
		String candi = SIO.next();
		if(interactive) System.out.println("Introdueix la solucio: ");
		String solucio = SIO.next();
        Jugada j2 = new Jugada(candi,solucio);
		System.out.println("nova jugada creada amb candidat: " + j2.getCandidat() + " y BN: " + j2.getBN());
    }  
    
    public void testgetCandidat(){
        if(interactive) System.out.println("Introdueix el candidat: ");
		String candi = SIO.next();
		if(interactive) System.out.println("Introdueix la solucio: ");
		String solucio = SIO.next();
        Jugada j2 = new Jugada(candi,solucio);
		System.out.println("el candidat es: " + j2.getCandidat());
    }
  
    public void testgetBN(){
        if(interactive) System.out.println("Introdueix el candidat: ");
		String candi = SIO.next();
		if(interactive) System.out.println("Introdueix la solucio: ");
		String solucio = SIO.next();
        Jugada j2 = new Jugada(candi,solucio);
		System.out.println("les BN son: " + j2.getBN());
    }
	
	public void usage(){
		System.out.println("\nusage: ");
		System.out.println("  testjugada");
		System.out.println("  testgetCandidat");
		System.out.println("  testgetBN");
		System.out.println("  exit");
		System.out.println("  usage\n");
	}

}
