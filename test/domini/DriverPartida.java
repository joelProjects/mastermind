package test.domini;
import java.util.*;
import java.io.File;
import java.io.FileNotFoundException;
/**
 *
 * @author Oliver
 */
public class DriverPartida {
	
	Scanner SIO;
	boolean interactive;
    
    public static void main (String[] args) throws Exception{
		Scanner S;
		boolean inter;
		if(args.length>0){
			File file = new File(args[0]);
			S=new Scanner(file);
			inter=false;
		}
		else{
			S=new Scanner(System.in);
			inter=true;
		}		
		DriverPartida d = new DriverPartida(S,inter);
		if(inter) System.out.println("\nRecorda que pots pasar un fitxer com a parametre per ser llegit com a entrada");
		if(inter) d.usage();
		String op = S.next();
		while (!op.equals("exit")){
			switch(op){
				case "testPartida":					
					d.testPartida();
				break;
				case "testnovarondacodemaker":
					d.testnovarondacodemaker();
				break;
				case "testnovarondacodebreaker":
					d.testnovarondacodebreaker();
				break;
				case "testnovajugadacodebreaker":
					d.testnovajugadacodebreaker();
				break;
				case "testnovajugadacodemaker":
					d.testnovajugadacodemaker();
				break;
				case "testgetRonda":
					d.testgetRonda();
				break;
				case "testgetNrondes":
					d.testgetNrondes();
				break;
				case "testgetmode":
					d.testgetmode();
				break;
				case "testgetdificultat":
					d.testgetdificultat();
				break;
				case "testgetRolActual":
					d.testgetRolActual();
				break;
				case "testgetRolInicial":
					d.testgetRolInicial();
				break;
				case "testgetnom":
					d.testgetnom();
				break;
				default:
					d.usage();
				break;
			}
			op = S.next();
		}
	}
	
	public DriverPartida(Scanner S,boolean inter){
		SIO=S;
		interactive=inter;
	}
   
    public void testPartida(){
		if(interactive) System.out.println("Introdueix el mode(competitiu|practica): ");
		String m = SIO.next();
		if(interactive) System.out.println("Introdueix la dificultat(0|1|2): ");
		int d = SIO.nextInt();
		if(interactive) System.out.println("Introdueix el rol Inicial(0(CM)|1(CB)): ");
		int r = SIO.nextInt();
		if(interactive) System.out.println("Introdueix el nom: ");
		String n = SIO.next();
        Partida p = new Partida(m,d,r,n);
		System.out.println("nova Partida creada amb mode: " + p.getmode() + " , dificultat: " + p.getdificultat() + " , rolInicial: " + p.getRolInicial() + " y nom: " + p.getnom());
    } 
	
	public void testnovarondacodemaker()  throws Exception{
		if(interactive) System.out.println("Introdueix el mode(competitiu|practica): ");
		String m = SIO.next();
		if(interactive) System.out.println("Introdueix la dificultat(0|1|2): ");
		int d = SIO.nextInt();
		if(interactive) System.out.println("Introdueix el rol Inicial(0(CM)|1(CB)): ");
		int r = SIO.nextInt();
		if(interactive) System.out.println("Introdueix el nom: ");
		String n = SIO.next();
        Partida p = new Partida(m,d,r,n);
		if(interactive) System.out.println("Introdueix la solucio: ");
		String s = SIO.next();
		System.out.println("El candidat es: " + p.novarondacodemaker(s));
		
	}
	public void testnovarondacodebreaker(){
		if(interactive) System.out.println("Introdueix el mode(competitiu|practica): ");
		String m = SIO.next();
		if(interactive) System.out.println("Introdueix la dificultat(0|1|2): ");
		int d = SIO.nextInt();
		if(interactive) System.out.println("Introdueix el rol Inicial(0(CM)|1(CB)): ");
		int r = SIO.nextInt();
		if(interactive) System.out.println("Introdueix el nom: ");
		String n = SIO.next();
        Partida p = new Partida(m,d,r,n);
		p.novarondacodebreaker();		
	}
	public void testnovajugadacodebreaker() throws Exception{
		if(interactive) System.out.println("Introdueix el mode(competitiu|practica): ");
		String m = SIO.next();
		if(interactive) System.out.println("Introdueix la dificultat(0|1|2): ");
		int d = SIO.nextInt();
		if(interactive) System.out.println("Introdueix el rol Inicial(0(CM)|1(CB)): ");
		int r = SIO.nextInt();
		if(interactive) System.out.println("Introdueix el nom: ");
		String n = SIO.next();
        Partida p = new Partida(m,d,r,n);
		p.novarondacodebreaker();	
		if(interactive) System.out.println("Introdueix el candidat: ");
		String c = SIO.next();
		System.out.println("Les BN son: " + p.novajugadacodebreaker(c));
	}
	public void testnovajugadacodemaker() throws Exception{
		if(interactive) System.out.println("Introdueix el mode(competitiu|practica): ");
		String m = SIO.next();
		if(interactive) System.out.println("Introdueix la dificultat(0|1|2): ");
		int d = SIO.nextInt();
		if(interactive) System.out.println("Introdueix el rol Inicial(0(CM)|1(CB)): ");
		int r = SIO.nextInt();
		if(interactive) System.out.println("Introdueix el nom: ");
		String n = SIO.next();
        Partida p = new Partida(m,d,r,n);
		if(interactive) System.out.println("Introdueix la solucio: ");
		String s = SIO.next();
		System.out.println("El candidat es: " + p.novarondacodemaker(s));
		if(interactive) System.out.println("Introdueix les BN: ");
		String bn = SIO.next();
		System.out.println("El seguent candidat es: " + p.novajugadacodemaker(bn));
	}
	public void testgetRonda(){
		if(interactive) System.out.println("Introdueix el mode(competitiu|practica): ");
		String m = SIO.next();
		if(interactive) System.out.println("Introdueix la dificultat(0|1|2): ");
		int d = SIO.nextInt();
		if(interactive) System.out.println("Introdueix el rol Inicial(0(CM)|1(CB)): ");
		int r = SIO.nextInt();
		if(interactive) System.out.println("Introdueix el nom: ");
		String n = SIO.next();
        Partida p = new Partida(m,d,r,n);
		p.getRonda(0);
	}
	public void testgetNrondes(){
		if(interactive) System.out.println("Introdueix el mode(competitiu|practica): ");
		String m = SIO.next();
		if(interactive) System.out.println("Introdueix la dificultat(0|1|2): ");
		int d = SIO.nextInt();
		if(interactive) System.out.println("Introdueix el rol Inicial(0(CM)|1(CB)): ");
		int r = SIO.nextInt();
		if(interactive) System.out.println("Introdueix el nom: ");
		String n = SIO.next();
		Partida p = new Partida(m,d,r,n);
		System.out.println("Hi han "+ p.getNrondes() +" rondes");
	}
	public void testgetmode(){
		if(interactive) System.out.println("Introdueix el mode(competitiu|practica): ");
		String m = SIO.next();
		if(interactive) System.out.println("Introdueix la dificultat(0|1|2): ");
		int d = SIO.nextInt();
		if(interactive) System.out.println("Introdueix el rol Inicial(0(CM)|1(CB)): ");
		int r = SIO.nextInt();
		if(interactive) System.out.println("Introdueix el nom: ");
		String n = SIO.next();
		Partida p = new Partida(m,d,r,n);
		System.out.println("El mode es: " + p.getmode() );
	}
	public void testgetdificultat(){
		if(interactive) System.out.println("Introdueix el mode(competitiu|practica): ");
		String m = SIO.next();
		if(interactive) System.out.println("Introdueix la dificultat(0|1|2): ");
		int d = SIO.nextInt();
		if(interactive) System.out.println("Introdueix el rol Inicial(0(CM)|1(CB)): ");
		int r = SIO.nextInt();
		if(interactive) System.out.println("Introdueix el nom: ");
		String n = SIO.next();
		Partida p = new Partida(m,d,r,n);
		System.out.println("La dificultat es: " + p.getdificultat() );
	}
	public void testgetRolActual(){
		if(interactive) System.out.println("Introdueix el mode(competitiu|practica): ");
		String m = SIO.next();
		if(interactive) System.out.println("Introdueix la dificultat(0|1|2): ");
		int d = SIO.nextInt();
		if(interactive) System.out.println("Introdueix el rol Inicial(0(CM)|1(CB)): ");
		int r = SIO.nextInt();
		if(interactive) System.out.println("Introdueix el nom: ");
		String n = SIO.next();
		Partida p = new Partida(m,d,r,n);
		p.novarondacodebreaker();
		System.out.println("El rolActual es: " + p.getRolActual() );
	}
	
	public void testgetRolInicial(){
		if(interactive) System.out.println("Introdueix el mode(competitiu|practica): ");
		String m = SIO.next();
		if(interactive) System.out.println("Introdueix la dificultat(0|1|2): ");
		int d = SIO.nextInt();
		if(interactive) System.out.println("Introdueix el rol Inicial(0(CM)|1(CB)): ");
		int r = SIO.nextInt();
		if(interactive) System.out.println("Introdueix el nom: ");
		String n = SIO.next();
		Partida p = new Partida(m,d,r,n);
		System.out.println("El rolInicial es: " + p.getRolInicial() );
	}
	
	public void testgetnom(){
		if(interactive) System.out.println("Introdueix el mode(competitiu|practica): ");
		String m = SIO.next();
		if(interactive) System.out.println("Introdueix la dificultat(0|1|2): ");
		int d = SIO.nextInt();
		if(interactive) System.out.println("Introdueix el rol Inicial(0(CM)|1(CB)): ");
		int r = SIO.nextInt();
		if(interactive) System.out.println("Introdueix el nom: ");
		String n = SIO.next();
		Partida p = new Partida(m,d,r,n);
		System.out.println("El nom es: " + p.getnom() );
	}
	
	
	public void usage(){
		System.out.println("\nusage: ");
		System.out.println("  testPartida");
		System.out.println("  testnovarondacodemaker");
		System.out.println("  testnovarondacodebreaker");
		System.out.println("  testnovajugadacodebreaker");
		System.out.println("  testnovajugadacodemaker");
		System.out.println("  testgetRonda");
		System.out.println("  testgetNrondes");
		System.out.println("  testgetmode");
		System.out.println("  testgetdificultat");
		System.out.println("  testgetRolActual");
		System.out.println("  testgetRolInicial");
		System.out.println("  testgetnom");
		System.out.println("  exit");
		System.out.println("  usage\n");
	}

}
