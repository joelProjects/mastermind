package mastermind.domini;

/**
 * 
 * @author Joel Moreno
 */

import java.io.*;
import java.util.*;

public class controlMain {

    public static void main(String[] args) {
        boolean exception = true;
        ControladorDomini control = new ControladorDomini();
        Console console = System.console();
        String nom, mode, difficulty, roleplay;
        nom = mode = difficulty = roleplay = "";
        int dif, rol, rondes;
        dif = rol = rondes = -1;
        
        while(exception){
            try{
                nom = console.readLine("introduiu el nom: ");
                mode = console.readLine("introduiu el mode practica o competitiu: ");
                difficulty = console.readLine("introduiu la dificultat 0, 1 o 2: ");
                dif = difficulty.charAt(0) - '0';
                roleplay = console.readLine("introduiu el rol codemaker(0) o codebreaker(1): ");
                rol = roleplay.charAt(0) - '0';
                exception = false;
                control.crearPartida(nom, dif, mode, rol);
                if(mode.compareTo("practica") == 0) rondes = 1;
                else rondes = 4;
            }
            catch(Exception e){
                exception = true;
                System.out.println("Exception: " + e.getMessage());

            }
        }
        for(int j = 0; j < rondes; ++j){
            if(rol == 1){
                String bn = "";
                String candid = "";
                int jugades = 1;
                boolean totNegres = false;
                while(!totNegres && jugades <= 12){
                    exception = true;
                    while(exception){
                        try{
                            exception = false;
                            candid = console.readLine("Introduiu el candidat de la jugada " + jugades + ": ");
                            
                            bn = control.jugar(candid);
                            System.out.println("BN: " + bn);
                            ++jugades;
                        
                            if((bn.length() == 3 && dif == 0) || (bn.length() == 4 && dif == 1) || (bn.length() == 5 && dif == 2)){
                                totNegres = true;
                                for(int k = 0; k < bn.length(); ++k){
                                    if(bn.charAt(k) != 'N') totNegres = false;
                                }
                            }
                        }
                        catch(Exception e){
                            exception = true;
                            System.out.println("Exception: " + e.getMessage());
                        }
                    }
                }
                if(!totNegres) System.out.println("Has perdut la ronda");
                else System.out.println("Has guanyat la ronda");
                control.finalitzarRonda();
            }
            else{
                String solucio = "";
                String candidat = "";
                String blancnegre = "";
                int jugades = 1;
                exception = true;
                while(exception){
                    try{
                        exception = false;
                        solucio = console.readLine("Introduiu la solucio: ");
                        
                        candidat = control.creaRondaCodemaker(solucio);
                    }
                    catch(Exception e){
                        exception = true;
                        System.out.println("Exception: " + e.getMessage());
        
                    }
                }

                while(! solucio.equals(candidat) && jugades < 12){
                    exception = true;
                    while(exception){
                        try{
                            System.out.println("Candidat de jugada " + (jugades) + ": " + candidat);
                            exception = false;
                            blancnegre = console.readLine("Introduiu les blanques i negres: ");
                            candidat = control.jugar(blancnegre);
                            ++jugades;
                        }
                        catch(Exception e){
                            exception = true;
                            System.out.println("Exception: " + e.getMessage());
                            
                        }
                    }
                }
                System.out.println("Candidat de jugada " + (jugades) + ": " + candidat);
                if(! solucio.equals(candidat)) System.out.println("Has guanyat la ronda");
                else{
                    exception = true;
                    while(exception){
                        try{
                            exception = false;
                            String lastBN = console.readLine("Introduiu les blanques i negres: ");
                            if(dif == 0 && lastBN.compareTo("NNN") != 0) throw new Exception("El jugador no esta introduciendo las BN correctamente");
                            if(dif == 1 && lastBN.compareTo("NNNN") != 0) throw new Exception("El jugador no esta introduciendo las BN correctamente");
                            if(dif == 2 && lastBN.compareTo("NNNNN") != 0) throw new Exception("El jugador no esta introduciendo las BN correctamente");
                        }
                        catch(Exception e){
                            exception = true;
                            System.out.println(e.getMessage());
                        }
                    }
                    System.out.println("Has perdut la ronda");
                }
                control.finalitzarRonda();
            }
            if(rol == 0) ++rol;
            else --rol;
        }

		System.out.println("THE END");

    }
}
