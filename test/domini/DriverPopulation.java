package test.domini;

/**
 * 
 * @author Joan Llop
 */

import java.util.*;

public class DriverPopulation {
	public static void main (String[] args){
		usage();
		boolean a = true;
		int op = 4;
		while (a) {
			try {
				op = Integer.parseInt(System.console().readLine("\nIntroduiu opcio: "));
				a = false;
			}
			catch (Exception e){
				System.out.println("Exception: " + e.getMessage() + "\n\tl'opcio ha de ser un numero");
			}
		}
		while (op != 5){
			switch(op){
				case 1:
					if (testCreator()) System.out.println("==========ok==========");
					else System.out.println("==========TEST FAIL==========");
				break;
				case 2:
					if (testGetFittest()) System.out.println("==========ok==========");
					else System.out.println("==========TEST FAIL==========");
				break;
				case 3:
					if (testSort()) System.out.println("==========ok==========");
					else System.out.println("==========TEST FAIL==========");
				break;
				default:
				usage();
			}
			a = true;
			while (a) {
				try {
					op = Integer.parseInt(System.console().readLine("\nIntroduiu opcio: "));
					a = false;
				}
				catch (Exception e){
					System.out.println("Exception: " + e.getMessage() + "\n\tl'opcio ha de ser un numero");
				}
			}
		}
	}

	private static boolean testCreator() {
		boolean r = true;		
		try {
			String Pre = "Pre: llargada dels codis < 10\n";
			Pre += "\tnumero de colors < 10\n";
			System.out.println("Provant la funcio creadora...\n" + Pre);
			int nC = Integer.parseInt(System.console().readLine(">>>Introduiu el numero de colors: "));
			int nH = Integer.parseInt(System.console().readLine(">>>Introduiu la llargada dels Individus: "));
			int nP = Integer.parseInt(System.console().readLine(">>>Introduiu la mida de la poblacio: "));
			Population myPop = new Population(nP, nC, nH);
			for (int i = 0; i < nP; ++i) System.out.println("\t\t" + myPop.getIndividual(i).toString());
		}
		catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
			r = false;
		}
		return r;
	}
	private static boolean testGetFittest() {
		boolean r = true;		
		try {
			String Pre = "Pre: llargada dels codis < 10\n";
			Pre += "\tnumero de colors < 10\n";
			System.out.println("Provant la funcio creadora...\n" + Pre);
			int nC = Integer.parseInt(System.console().readLine(">>>Introduiu el numero de colors: "));
			int nH = Integer.parseInt(System.console().readLine(">>>Introduiu la llargada dels Individus: "));
			int nP = Integer.parseInt(System.console().readLine(">>>Introduiu la mida de la poblacio: "));
			Population myPop = new Population(nP, nC, nH);
			for (int i = 0; i < nP; ++i) System.out.println("\t\t" + myPop.getIndividual(i).toString());
			System.out.println("fittest (sempre sera el primer, ja que l'StubFitnessCalc nomes retorna 0): " + myPop.getFittest().toString());
		}
		catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
			r = false;
		}
		return r;
	}

	private static boolean testSort() {
		boolean r = true;		
		try {
			String Pre = "Pre: llargada dels codis < 10\n";
			Pre += "\tnumero de colors < 10\n";
			System.out.println("Provant la funcio creadora...\n" + Pre);
			int nC = Integer.parseInt(System.console().readLine(">>>Introduiu el numero de colors: "));
			int nH = Integer.parseInt(System.console().readLine(">>>Introduiu la llargada dels Individus: "));
			int nP = Integer.parseInt(System.console().readLine(">>>Introduiu la mida de la poblacio: "));
			Population myPop = new Population(nP, nC, nH);
			System.out.println("abans d'ordenar: ");
			for (int i = 0; i < nP; ++i) System.out.println("\t\t" + myPop.getIndividual(i).toString());
			myPop.sort();
			System.out.println("despres d'ordenar: ");
			for (int i = 0; i < nP; ++i) System.out.println("\t\t" + myPop.getIndividual(i).toString());
		}
		catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
			r = false;
		}
		return r;
	}
 

	private static void usage (){
		System.out.println("\nUsage:\n1 - test creadora i addRandom\n2 - test GetFittest\n3 - test sort\n4 - usage\n5 - exit\n");
	}
}