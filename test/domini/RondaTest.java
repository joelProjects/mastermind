package test.domini;

import java.util.ArrayList;
import java.util.Random;
import static org.junit.Assert.*;
import org.junit.Test;
        
/**
 *
 * @author Oliver 
 */
public class RondaTest {

	@Test
    public void T1Ronda(){
        Ronda r = new Ronda(0);
        String solucio = r.getsolucio();
		assertTrue("T1",solucio.length()==3);		
    }
    
	@Test
    public void T2Ronda(){
        Ronda r = new Ronda(1);
        String solucio = r.getsolucio();
		assertTrue("T2",solucio.length()==4);		
    }
	
	@Test
    public void T3Ronda(){
        Ronda r = new Ronda(2);
        String solucio = r.getsolucio();
		assertTrue("T3",solucio.length()==5);		
    }
    
    
}
