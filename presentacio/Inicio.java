/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mastermind.presentacio;


import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;


/**
 *
 * @author Olive
 */
public class Inicio implements Initializable {
    
    
    @FXML
    private void handlenueva(ActionEvent event) throws Exception {
        Parent home_page_parent = FXMLLoader.load(getClass().getResource("NuevaPartida.fxml"));
        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.setScene(home_page_scene);
        app_stage.setResizable(false);
        app_stage.show();  
    }
	@FXML
    private void handlehowtoplay(ActionEvent event) throws Exception {
        Parent home_page_parent = FXMLLoader.load(getClass().getResource("HowToPlay.fxml"));
        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.setScene(home_page_scene);
        app_stage.setResizable(false);
        app_stage.show();  
    }
    @FXML
    private void handlecargar(ActionEvent event) throws Exception {
        Parent home_page_parent = FXMLLoader.load(getClass().getResource("CargarPartida.fxml"));
        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.setScene(home_page_scene);
        app_stage.setResizable(false);
        app_stage.show();  
    }
    @FXML
    private void handlerankings(ActionEvent event)throws Exception {
        Parent home_page_parent = FXMLLoader.load(getClass().getResource("Rankings.fxml"));
        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.setScene(home_page_scene);
        app_stage.setResizable(false);
        app_stage.show();  
    }

    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
