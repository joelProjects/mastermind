/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mastermind.presentacio;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.stage.Stage;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TextField;
import java.lang.String;
import javafx.application.Platform;
import java.util.concurrent.ScheduledExecutorService;
import java.lang.management.ManagementFactory;
import javafx.scene.image.Image;

/**
 *
 * @author Olive
 */
public class Menu extends Application {
    
	
    @Override
    public void start(Stage stage) throws Exception {
		stage.setResizable(false);
        // Platform.setImplicitExit(false);
        stage.setOnCloseRequest(e -> {
            // System.out.println("adeu1");
            Platform.exit();
            stage.close();
            // System.exit(0);
        });
        Parent root = FXMLLoader.load(getClass().getResource("Inicio.fxml"));
        
        Scene scene = new Scene(root);
        stage.getIcons().add(new Image("mastermind/presentacio/icon.jpg"));
        stage.setTitle("Mastermind");
        stage.setScene(scene);
        stage.show();
        
    }

    @Override
    public void stop() {
        System.exit(0);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
