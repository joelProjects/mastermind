package mastermind.presentacio;


import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
// import javafx.scene.layout.AnchorPane;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
// import mastermind.presentacio.Tauler;
import javafx.scene.layout.Pane;
import javafx.embed.swing.SwingNode;
import javax.swing.SwingUtilities;
import java.time.LocalDateTime;
import javax.swing.*;
import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import javax.sound.sampled.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.event.WindowAdapter;
import java.awt.image.BufferedImage;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import javafx.application.Platform;


/**
 *
 * @author Joan
 */
public class Taula implements Initializable {
    
    @FXML
    private Pane pane;

    private int diff;

    private boolean mode;

    private boolean rol;

    // private static Dimension OUTER_FRAME_DIMENSION = new Dimension(100, 100);
	
    private mySwingNode swingNode;

    public class mySwingNode extends SwingNode {
        public boolean isResizable(){return false;}
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        diff = ControladorPresentacio.dificultad;
        mode = ControladorPresentacio.modo.equals("practica");
        rol = ControladorPresentacio.rol == 1;
        // System.out.println("diff: " +diff + "\nmode: " + mode + "\nrol: " + rol);
        swingNode = new mySwingNode();
		createAndSetSwingContent(swingNode);
		
		pane.getChildren().add(swingNode); // Adding swing node

        pane.widthProperty().addListener((obs, oldVal, newVal) -> {
            swingNode.resize(newVal.doubleValue(), pane.getHeight());
        });
        pane.heightProperty().addListener((obs, oldVal, newVal) -> {
            swingNode.resize(pane.getWidth(), newVal.doubleValue());
        });

    }
    
	private void createAndSetSwingContent(final SwingNode swingNode) {
         SwingUtilities.invokeLater(new Runnable() {
             @Override
             public void run() {
                Paint tauler = new Paint();
                 swingNode.setContent(tauler);
             }
         });
     }

    class Paint extends JPanel implements ActionListener {

        private int NUM_COLORS;
        private int NUM_HOLES;

        private Dimension gameSize;

        private Ellipse2D[] actual_circles = new Ellipse2D[4];
        private Ellipse2D[] actual_bn = new Ellipse2D[4];

        private Ellipse2D BackButton;
        private Ellipse2D verifyButton;
        private Ellipse2D helpButton;
        private boolean help_on = false;
        private BufferedImage help_image_1;
        private BufferedImage help_image_2;
        private BufferedImage help_image_3;
        private BufferedImage help_image_4;
        private BufferedImage help_image_5;
        private BufferedImage BackButtonImage;
        private BufferedImage VerifyButtonImage;
        private BufferedImage BackgroundImage;

        private BufferedImage mute_image;
        private BufferedImage volume_image;
        private BufferedImage volume_up_image;
        private BufferedImage volume_down_image;
        private Ellipse2D muteButton;
        private Ellipse2D volumeUpButton;
        private Ellipse2D volumeDownButton;
        private FloatControl gainControl;
        private Sound s;
        private boolean sound_paused = false;
        private float volume_control;

        
        private int x_mouse, y_mouse;

        private int num_jugada = 0;

        private int[][] posicio_color = new int[12][4];
        private int[][] bn_posicio = new int[12][4];
        // private Ellipse2D Blanc;
        // private Ellipse2D Negre;

        private boolean init = true;        
        private boolean end = false;
        

        private Timer timer;

        private boolean is_clicked = false;
        private boolean is_pressed = false;
        private int x_pressed;
        private int y_pressed;
        private boolean oneClick = false;

        
        private int colorPressed = -1;
        private Ellipse2D[] Colors = new Ellipse2D[8];
        private boolean B_pressed = false;
        private boolean N_pressed = false;

        private Ellipse2D[] Solution_circles = new Ellipse2D[4];
        private int[] solution_color = new int[4];
        private boolean solution_set = false;

        private MyMouseListener mL;
        private MyMouseMotionListener mML; 


        @Override
        protected void paintComponent(Graphics g2) {
            Graphics2D g = (Graphics2D) g2;
            super.paintComponent(g);
            // super.setBackground(new Color(30, 33, 36));
            gameSize = getPreferredSize();
            
            // String circlePath = "mastermind/presentacio/circle.png";
            if (init){
                // System.out.println("init");
                timer = new Timer(9, this);
                timer.addActionListener(this);
                timer.start();
                volume_control = 0.75f;
                help_image_1 = load_image("mastermind/presentacio/help_image_1.png");
                help_image_2 = load_image("mastermind/presentacio/help_image_2.png");
                help_image_3 = load_image("mastermind/presentacio/help_image_3.png");
                help_image_4 = load_image("mastermind/presentacio/help_image_4.png");
                mute_image = load_image("mastermind/presentacio/mute-button.png");
                volume_image = load_image("mastermind/presentacio/volume-button.png");
                volume_up_image = load_image("mastermind/presentacio/volume_up_button.png");
                volume_down_image = load_image("mastermind/presentacio/volume_down_button.png");
                BackgroundImage = load_image("mastermind/presentacio/background.jpg");
                sound();
                colorPressed = -1;  
                mL = new MyMouseListener();
                mML = new MyMouseMotionListener();
                addMouseListener(mL);
                addMouseMotionListener(mML);
                set_num_colors_and_num_holes();
                actual_circles = new Ellipse2D[NUM_HOLES];
                actual_bn = new Ellipse2D[NUM_HOLES];
                posicio_color = new int[12][NUM_HOLES];
                bn_posicio = new int[12][NUM_HOLES];
                Solution_circles = new Ellipse2D[NUM_HOLES];
                solution_color = new int[NUM_HOLES];
                set_posicio_color_and_bn_m1();
                set_solution_color_m1();
                BackButtonImage = load_image("mastermind/presentacio/back-button.png");
                // VerifyButtonImage = load_image("mastermind/presentacio/verifyButton.png");
                if (!ControladorPresentacio.nueva) carrega_matrius();
                //load images;
                init = false;
            }
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
            g.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
            g.drawImage(BackgroundImage, 0, 0, gameSize.width, gameSize.height, null);
            paint_help_button(g);
            paint_mute_button_and_up_down(g);
            paint_grey_background(g);
            paint_back_button(g);
            paint_verify_button(g);
            paint_hour_minutes_seconds(g);
            if (help_on){
                print_help(g);
            }
            if (rol){
                paint_circles_and_BN(g);
                if (!end){
                    create_elipses_colors(); //no es pot fer a l'inici per el resize.
                    DrowColors(g);
                    paint_mouse_pressed_color(g);
                }
            }
            else{
                paint_solution_circles(g);
                if (solution_set){
                    paint_circles_and_BN(g);
                    // paint_BN(g);
                    paint_mouse_pressed_BN(g);
                }
                else{
                    create_elipses_colors();
                    DrowColors(g);
                    paint_mouse_pressed_color(g);
                }
            }
            
        }

        private void print_help(Graphics2D g){
            int x = gameSize.width*30/36;
            int y = gameSize.height*3/10;
            int costat = Math.min(gameSize.width*5/36, gameSize.height*5/10);
            if (!end){
                if (rol){
                    g.drawImage(help_image_1, x, y, costat, costat, null);
                }
                else{
                    if (!solution_set){
                        g.drawImage(help_image_3, x, y, costat, costat, null);
                    }
                    else{
                        g.drawImage(help_image_2, x, y, costat, costat, null);
                    }
                }
            }
            else{
                g.drawImage(help_image_4, x, y, costat, costat, null);
            }

        }

        private void paint_help_button(Graphics2D g){
            int centre_x = gameSize.width*7/9;
            int centre_y = Math.max(Math.min(gameSize.height*3/10, gameSize.height/2 - gameSize.width/20), 0);
            int radi = gameSize.width/20;
            helpButton = new Ellipse2D.Double(centre_x, centre_y, radi, radi);
            g.setColor(new Color(85, 85, 85, 255));
            g.setStroke(new BasicStroke(2));
            g.fill(helpButton);
            g.setColor(new Color(30, 33, 36, 255));
            g.setFont(new Font("Lora", Font.PLAIN + Font.BOLD, radi/2));
            FontMetrics metrics = g.getFontMetrics(g.getFont());
            g.drawString("?", centre_x +(radi/2) - metrics.stringWidth("?")/2, centre_y + radi*22/32);

        }

        private void paint_back_button(Graphics2D g){
            g.setColor(new Color(85, 85, 85, 255));
            g.setStroke(new BasicStroke(3));
            BackButton = new Ellipse2D.Double(gameSize.width/50, gameSize.height/50, gameSize.width/20, gameSize.width/20);
            g.drawImage(BackButtonImage, gameSize.width/50, gameSize.height/50, gameSize.width/20, gameSize.width/20, null);
            g.draw(BackButton);
            g.setStroke(new BasicStroke(2));
            g.setColor(new Color(0, 0, 0, 255));
        }

        private void paint_verify_button(Graphics2D g){
            verifyButton = new Ellipse2D.Double(gameSize.width*7/9, gameSize.height/2, gameSize.width/20, gameSize.width/20);
            if (end){
                g.drawImage(VerifyButtonImage, gameSize.width*7/9, gameSize.height/2, gameSize.width/20, gameSize.width/20, null);
            }
            else {
                g.setColor(new Color(79, 186, 111, 255));
                g.fill(verifyButton);
                g.setColor(new Color(255, 255, 255, 255));
                g.setStroke(new BasicStroke(2));
                int x1 = gameSize.width*7/9  + gameSize.width/20*4/10;
                int y1 = gameSize.height/2 + gameSize.width/20/4;
                int x2 = gameSize.width*7/9 + gameSize.width/20*13/20;
                int y2 = gameSize.height/2 + gameSize.width/20/2;
                int x3 = gameSize.width*7/9  + gameSize.width/20*4/10;
                int y3 = gameSize.height/2 + gameSize.width/20*3/4;
                g.drawLine(x1, y1, x2, y2);
                g.drawLine(x2, y2, x3, y3);
                g.setColor(new Color(0, 0, 0, 255));
            }
        }

        private void paint_hour_minutes_seconds(Graphics2D g){
            LocalDateTime now = LocalDateTime.now();
            g.setColor(new Color(85, 85, 85, 255));
            int hour = now.getHour();
            int min = now.getMinute();
            int sec = now.getSecond();
            int newsec=(sec+1)%60;
            int newmin=(min+(sec+1)/60)%60;
            hour=(hour+(min+(sec+1)/60)/60)%12;
            sec=newsec;
            min=newmin;

            double minsecdeg=(double)Math.PI/30;
            double hrdeg=(double)Math.PI/6;
            int tx,ty;
            int xpoints[]=new int[3];
            int ypoints[]=new int[3];

            int clock_center_x = (gameSize.width - (gameSize.width*(16+6-diff*3)/60 + gameSize.width*(29-10+diff*5)/60))/2 + (gameSize.width*(16+6-diff*3)/60 + gameSize.width*(29-10+diff*5)/60);
            int clock_center_y = gameSize.height*3/20;
            int s_large = Math.min(gameSize.height/10, gameSize.width/10);
            int m_large = Math.min(gameSize.height/15, gameSize.width/15);
            int h_large = Math.min(gameSize.height/20, gameSize.width/20);

            g.setStroke(new BasicStroke(1));

            //second hand
            tx=clock_center_x+(int)(s_large*Math.sin(sec*minsecdeg));
            ty=clock_center_y-(int)(s_large*Math.cos(sec*minsecdeg));
            g.drawLine(clock_center_x,clock_center_y,tx,ty);

            //minute hand
            tx=clock_center_x+(int)(m_large*Math.sin(min*minsecdeg));
            ty=clock_center_y-(int)(m_large*Math.cos(min*minsecdeg));
            xpoints[0]=clock_center_x;
            xpoints[1]=tx+2;
            xpoints[2]=tx-2;
            ypoints[0]=clock_center_y;
            ypoints[1]=ty+2;
            ypoints[2]=ty-2;
            g.fillPolygon(xpoints, ypoints,3);

            //hour hand
            tx=clock_center_x+(int)(h_large*Math.sin(hour*hrdeg+min*Math.PI/360));
            ty=clock_center_y-(int)(h_large*Math.cos(hour*hrdeg+min*Math.PI/360));
            xpoints[1]=tx+2;
            xpoints[2]=tx-2;
            ypoints[1]=ty-2;
            ypoints[2]=ty+2;
            g.fillPolygon(xpoints, ypoints, 3);
            g.setColor(new Color(0, 0, 0, 255));
        }

        private void paint_grey_background(Graphics2D g){
            g.setColor(new Color(66, 69, 73, 155));
            g.fill(new Rectangle2D.Double(gameSize.width*(16+6-diff*3)/60, gameSize.height*1/20, gameSize.width*(29-10+diff*5)/60, gameSize.height*17/20));
            g.setColor(new Color(107, 106, 101, 255));
            g.setStroke(new BasicStroke(1));
            g.drawRect(gameSize.width*(16+6-diff*3)/60, gameSize.height*1/20, gameSize.width*(29-10+diff*5)/60, gameSize.height*17/20);
            g.setColor(new Color(0, 0, 0, 255));
        }

        private void paint_circles_and_BN(Graphics2D g){
            for (int i = 0; /*(end && i < 12) || i <= num_jugada &&*/ i < 12; ++i){
                addCirclesLine(g, 12-i);
                addBlanquesNegres(g, 12-i);
                for (int k = 0; k < NUM_HOLES; ++k){
                    if (posicio_color[i][k] != -1){
                        DrowColor(g, posicio_color[i][k], gameSize.width*(k+(8-diff))/20, gameSize.height*(12-i)/15, Math.min(gameSize.height/20, gameSize.width/20));
                    }
                    if (bn_posicio[i][k] != -1){
                        DrowBN(g, bn_posicio[i][k], gameSize.width*(k+17)/30, gameSize.height*(12-i)/15, Math.min(gameSize.height/35, gameSize.width/35));
                    }
                }
            }
        }

        private void paint_mouse_pressed_color(Graphics2D g){
            for (int i=0; i < NUM_COLORS; ++i){
                if (colorPressed != -1)
                    DrowColor(g, colorPressed, x_mouse-(gameSize.height*1/40), y_mouse-(gameSize.height*1/40), (gameSize.height*1/20));
            }
        }

        private BufferedImage load_image(String path){
            try {
                File file = new File(path);
                return ImageIO.read(file);
            } catch (Exception e) {System.out.println(e.getMessage()); return null;}
        }

        private void set_num_colors_and_num_holes(){
            if (diff == 0){
                NUM_COLORS = 5;
                NUM_HOLES = 3;
            }
            else if (diff == 1){
                NUM_COLORS = 6;
                NUM_HOLES = 4;
            }
            else{
                NUM_COLORS = 8;
                NUM_HOLES = 5;
            }
        }

        private void DrowBN(Graphics2D g, int color, int x_pos, int y_pos, int mida){
            switch (color) {
                case 0: g.setColor(new Color(255, 255, 255)); //vermell
                        g.fill(new Ellipse2D.Double(x_pos, y_pos, mida, mida));
                        break;
                case 1: g.setColor(new Color(0, 0, 0)); //blau
                        g.fill(new Ellipse2D.Double(x_pos, y_pos, mida, mida));
                        break;
                default:
                    System.out.println("index out of bounds");
                    break;
            }
            
        }

        private void DrowColor(Graphics2D g, int color, int x_pos, int y_pos, int mida){
            switch (color) {
                case 0: g.setColor(new Color(153,120,214)); 
                        g.fill(new Ellipse2D.Double(x_pos, y_pos, mida, mida));
                        break;
                case 1: g.setColor(new Color(146,189,59));
                        g.fill(new Ellipse2D.Double(x_pos, y_pos, mida, mida));
                        break;
                case 2: g.setColor(new Color(194,57,106)); 
                        g.fill(new Ellipse2D.Double(x_pos, y_pos, mida, mida));
                        break;
                case 3: g.setColor(new Color(99,188,206));
                        g.fill(new Ellipse2D.Double(x_pos, y_pos, mida, mida));
                        break;
                case 4: g.setColor(new Color(228,141,39)); 
                        g.fill(new Ellipse2D.Double(x_pos, y_pos, mida, mida));
                        break;
                case 5: g.setColor(new Color(232, 230, 146));
                        g.fill(new Ellipse2D.Double(x_pos, y_pos, mida, mida));
                        break;
                case 6: g.setColor(new Color(32, 117, 56));
                        g.fill(new Ellipse2D.Double(x_pos, y_pos, mida, mida));
                        break;
                case 7: g.setColor(new Color(48, 59, 204)); 
                        g.fill(new Ellipse2D.Double(x_pos, y_pos, mida, mida));
                        break;
                default:
                    System.out.println("index out of bounds");
                    break;
            }
            
        }

        // private void paint_BN(Graphics2D g) {
        //     int radi = Math.min(gameSize.height/35, gameSize.width/35);
        //     g.setColor(new Color(0, 0, 0, 0));
        //     Blanc = new Ellipse2D.Double(gameSize.width*27/35, gameSize.height*2/3, radi, radi);
        //     Negre = new Ellipse2D.Double(gameSize.width*29/35, gameSize.height*2/3, radi, radi);
        //     paint_Blanc(g, gameSize.width*27/35, gameSize.height*2/3, radi);
        //     paint_Negre(g, gameSize.width*29/35, gameSize.height*2/3, radi);
        // }

        private void paint_mouse_pressed_BN(Graphics2D g){
            if (B_pressed){
                paint_Blanc(g, x_mouse-(gameSize.height/70), y_mouse-(gameSize.height/70), (gameSize.height/35));
            }
            else if (N_pressed){
                paint_Negre(g, x_mouse-(gameSize.height/70), y_mouse-(gameSize.height/70), (gameSize.height/35));
            }
            
        }

        private void paint_Blanc(Graphics2D g, int x, int y, int r){
            g.setColor(new Color(255, 255, 255, 255));
            g.fill(new Ellipse2D.Double(x, y, r, r));
        }

        private void paint_Negre(Graphics2D g, int x, int y, int r){
            g.setColor(new Color(0, 0, 0, 255));
            g.fill(new Ellipse2D.Double(x, y, r, r));
        }

        private void carrega_matrius(){
            String str = ControladorPresentacio.PartidaCargada;
            String solucio= str.substring(0,str.indexOf('|'));
            str= str.substring(str.indexOf('|')+1,str.length());
            num_jugada = 0;
            for (int i = 0; i < solucio.length(); ++i){
                solution_color[i] = Character.digit(solucio.charAt(i),10);
                solution_set = true;
            }
            while(str.length()>2){
                String candidat= str.substring(0,str.indexOf('$'));
                str= str.substring(str.indexOf('$')+1,str.length());
                String BN= str.substring(0,str.indexOf('$'));
                str= str.substring(str.indexOf('$')+1,str.length());
                for (int i = 0; i < candidat.length(); ++i){
                    posicio_color[num_jugada][i] = Character.digit(candidat.charAt(i),10);
                }
                if (str.length()>2 || rol == true){
                    for (int i = 0; i < BN.length(); ++i){
                        if (BN.charAt(i) == 'B') bn_posicio[num_jugada][i] = 0;
                        else if (BN.charAt(i) == 'N') bn_posicio[num_jugada][i] = 1;
                        else bn_posicio[num_jugada][i] = -1;
                    }
                }
                ++num_jugada;
            }
            if (rol){
                boolean ultima_ant = true;
                int aux = num_jugada;
                if (num_jugada > 0) --aux;
                for (int i = 0; i < NUM_HOLES; ++i){
                    if (bn_posicio[aux][i] != 1) ultima_ant = false;
                }
                if (ultima_ant){
                    s.stop();
                    ControladorPresentacio.finalitzarRonda();
                    num_jugada = 11;
                    end = true;
                    final_win_sound_and_button();
                }
            }
            if (!rol) --num_jugada;
        }

        private void create_elipses_colors(){
            int radi = (gameSize.height*1/20);
            for (int i = 0; i < 8; ++i){
                Colors[i] = new Ellipse2D.Double(gameSize.width*1/8, gameSize.height*(i+6)/20, radi, radi);
            }
        }

        private void DrowColors(Graphics2D g){
            g.setColor(new Color(153,120,214));
            g.fill(Colors[0]);
            g.setColor(new Color(146,189,59));
            g.fill(Colors[1]);
            g.setColor(new Color(194,57,106));
            g.fill(Colors[2]);
            g.setColor(new Color(99,188,206));
            g.fill(Colors[3]);
            g.setColor(new Color(228,141,39));
            g.fill(Colors[4]);
            if (NUM_COLORS >= 6){
                g.setColor(new Color(232, 230, 146));
                g.fill(Colors[5]);
                if (NUM_COLORS == 8){
                    g.setColor(new Color(32, 117, 56));
                    g.fill(Colors[6]);
                    g.setColor(new Color(48, 59, 204));
                    g.fill(Colors[7]);
                }
            }
        }

        private void set_solution_color_m1(){
            for (int i = 0; i < NUM_HOLES; ++i){
                solution_color[i] = -1;
            }
        }

        private void set_posicio_color_and_bn_m1(){
            for (int i = 0; i < NUM_HOLES; ++i){
                for(int p = 0; p < 12; ++p){
                    posicio_color[p][i] = -1;
                    bn_posicio[p][i] = -1;
                }
            }
        }

        private void addCirclesLine(Graphics2D g, int x){
            g.setStroke(new BasicStroke(2));
            int radi = Math.min(gameSize.height/20, gameSize.width/20);
            for(int i = 0; i < NUM_HOLES; ++i){
                int alpha = 1;
                if (posicio_color[-1*x+12][i] != -1) alpha = 0;

                if (num_jugada == x*(-1)+12){
                    actual_circles[i] = new Ellipse2D.Double(gameSize.width*(i+(8-diff))/20, gameSize.height*x/15, radi, radi);
                    if (rol) g.setColor(new Color(10, 10, 10, 255*alpha));
                    else g.setColor(new Color(5, 5, 5, 150*alpha));
                    g.draw(actual_circles[i]);
                }
                else {
                    g.setColor(new Color(5, 5, 5, 150*alpha));
                    g.draw(new Ellipse2D.Double(gameSize.width*(i+(8-diff))/20, gameSize.height*x/15, radi, radi));
                }
            }
            g.setColor(new Color(0, 0, 0, 250));
        }

        private void addBlanquesNegres(Graphics2D g, int x){
            g.setStroke(new BasicStroke(4));
            g.setColor(new Color(0, 0, 0, 75));
            
            int radi = Math.min(gameSize.height/35, gameSize.width/35);
            for(int i = 0; i < NUM_HOLES; ++i){
                int alpha = 1;
                if (bn_posicio[-1*x+12][i] != -1) alpha = 0;

                if (num_jugada == x*(-1)+12){
                    actual_bn[i] = new Ellipse2D.Double(gameSize.width*(i+17)/30, gameSize.height*x/15, radi, radi);
                    if (!rol) g.setColor(new Color(0, 0, 0, 75*alpha));
                    else g.setColor(new Color(20, 20, 20, 75*alpha));
                    g.draw(actual_bn[i]);
                }
                else {
                    g.setColor(new Color(20, 20, 20, 75*alpha));
                    g.draw(new Ellipse2D.Double(gameSize.width*(i+17)/30, gameSize.height*x/15, radi, radi));
                }
                g.setColor(new Color(0, 0, 0, 250));
            }
        }

        private int swap(int a, int b) {
           return a;
        }


        private void paint_solution_circles(Graphics2D g){
            g.setStroke(new BasicStroke(2));
            int radi = Math.min(gameSize.height/20, gameSize.width*(3)/60);
            for(int i = 0; i < NUM_HOLES; ++i){
                if (solution_color[i] != -1){
                    DrowColor(g, solution_color[i], gameSize.width*(12-diff*55/10+i*3)/60, gameSize.height*(12-num_jugada)/15, radi);
                    g.setColor(new Color(0, 0, 0, 0));
                }
                else g.setColor(new Color(255, 255, 255, 75));
                Solution_circles[i] = new Ellipse2D.Double(gameSize.width*(12-diff*55/10+i*3)/60, gameSize.height*(12-num_jugada)/15, radi, radi);
                g.draw(Solution_circles[i]);
            }
            g.setColor(new Color(0, 0, 0, 250));
        }

        @Override
        public Dimension getPreferredSize() {
            return new Dimension(Double.valueOf(pane.getWidth()).intValue(), Double.valueOf(pane.getHeight()).intValue());
        }

        public void removeNotify() {
            // System.out.println("adeu!");
            super.removeNotify();
            timer.stop();
            s.stop();
            removeMouseListener(mL);
            removeMouseMotionListener(mML);
        }

        @Override
        public void actionPerformed(java.awt.event.ActionEvent e) {
            repaint();
        }

        class MyMouseMotionListener extends MouseMotionAdapter {
            public void mouseDragged(MouseEvent e) {
                // System.out.println("dragged");
                x_mouse = e.getX();
                y_mouse = e.getY();
                if (!rol && solution_set && is_pressed && !end && (Math.abs(x_pressed - e.getX()) > gameSize.width/70 || Math.abs(y_pressed - e.getY()) > gameSize.height/70)) {
                    for (int i = 0; i < NUM_HOLES; ++i){
                        if (actual_bn[i].contains(x_pressed, y_pressed)) {
                            if (bn_posicio[num_jugada][i] == 0) {
                                B_pressed = true;
                                bn_posicio[num_jugada][i] = -1;
                            }
                            else if (bn_posicio[num_jugada][i] == 1){
                                N_pressed = true;
                                bn_posicio[num_jugada][i] = -1;
                            }                                    
                        }
                    }
                }
                repaint();
            }

            public void mouseMoved(MouseEvent e){   
                x_mouse = e.getX();
                y_mouse = e.getY();
                if (helpButton.contains(e.getX(), e.getY())){
                    help_on = true;
                }
                else help_on = false;
                repaint();
            }
        }

        class MyMouseListener extends MouseAdapter {
            public void mousePressed(MouseEvent e) {
                is_pressed = true;
                x_pressed = e.getX();
                y_pressed = e.getY();
                // System.out.println("pressed");
                x_mouse = e.getX();
                y_mouse = e.getY();
                if (!end){
                    if (rol) {
                        boolean click_somewhere = false;
                        for (int i = 0; i < NUM_HOLES; ++i){
                            if (actual_circles[i].contains(e.getX(), e.getY())) {
                                click_somewhere = true;
                                posicio_color[num_jugada][i] = swap(colorPressed, colorPressed = posicio_color[num_jugada][i]);
                            }
                        }
                        for (int i = 0; i < NUM_COLORS/* && !end*/; ++i){
                            if (Colors[i].contains(e.getX(), e.getY())) {
                                click_somewhere = true;
                                colorPressed = i;
                            }
                        }

                        if (!click_somewhere) colorPressed = -1;
                        

                        
                    
                        if (verifyButton.contains(e.getX(), e.getY()) && !is_clicked){
                            boolean complet = true;
                            String candidat = "";
                            for (int k = 0; k < NUM_HOLES; ++k){
                                if (posicio_color[num_jugada][k] == -1) complet = false;
                                candidat += posicio_color[num_jugada][k];
                            }
                            if (num_jugada <= 11 && complet){
                                try {
                                    String BN_aux = ControladorPresentacio.jugar(candidat);
                                    for (int i = 0; i < NUM_HOLES && i < BN_aux.length(); ++i){
                                        if (BN_aux.charAt(i) == 'B') bn_posicio[num_jugada][i] = 0;
                                        else if (BN_aux.charAt(i) == 'N') bn_posicio[num_jugada][i] = 1;
                                        else throw new Exception("merda");
                                    }
                                    if ((NUM_HOLES == 3 && BN_aux.equals("NNN")) || 
                                        (NUM_HOLES == 4 && BN_aux.equals("NNNN")) ||
                                        (NUM_HOLES == 5 && BN_aux.equals("NNNNN"))){
                                            
                                            ControladorPresentacio.finalitzarRonda();
                                            num_jugada = 11;
                                            end = true;
                                            s.stop();
                                            s.close();
                                            final_win_sound_and_button();
                                        }
                                    else if (num_jugada == 11){
                                        ControladorPresentacio.finalitzarRonda();
                                        end = true;
                                        s.stop();
                                        s.close();
                                        final_defeat_sound_and_button();
                                    }
                                    else {
                                        ++num_jugada;
                                    }
                                } catch (Exception exep) {
                                    System.out.println(exep.getMessage());
                                }
                            }
                            is_clicked = true;
                        }
                    } else {
                        if (!solution_set){
                            boolean click_somewhere = false;
                            for (int i = 0; i < NUM_COLORS; ++i){
                                if (Colors[i].contains(e.getX(), e.getY())) {
                                    colorPressed = i;
                                    click_somewhere = true;
                                }
                            }
                            for (int i = 0; i < NUM_HOLES; ++i){
                                if (Solution_circles[i].contains(e.getX(), e.getY())) {
                                    solution_color[i] = swap(colorPressed, colorPressed = solution_color[i]);
                                    click_somewhere = true;
                                }
                            }
                            if (!click_somewhere) colorPressed = -1;
                        }

                        if (verifyButton.contains(e.getX(), e.getY())){
                            if (!solution_set){
                                boolean complet = true;
                                String solucio = "";
                                for (int k = 0; k < NUM_HOLES; ++k){
                                    if (solution_color[k] == -1) complet = false;
                                    solucio += solution_color[k];
                                }
                                if (complet) {
                                    try {
                                        String candidat = ControladorPresentacio.creaRondaCodemaker(solucio);
                                        for (int i = 0; i < candidat.length(); ++i){
                                            posicio_color[num_jugada][i] = Character.digit(candidat.charAt(i),10);
                                        }
                                        solution_set = true;
                                        is_pressed = false;
                                    } catch (Exception exep) {System.out.println(exep.getMessage());}
                                }
                            }
                            else {
                                String bnng = "";
                                for (int i = 0; i < NUM_HOLES; ++i){
                                    if (bn_posicio[num_jugada][i] == 0) bnng += "B";
                                    else if (bn_posicio[num_jugada][i] == 1) bnng += "N";
                                }
                                try {
                                    boolean eq = true;
                                    for (int i = 0; i < NUM_HOLES; ++i){
                                        if (posicio_color[num_jugada][i] != solution_color[i]) eq = false;
                                    }
                                    if (eq){
                                        if ((NUM_HOLES == 3 && bnng.equals("NNN")) || 
                                            (NUM_HOLES == 4 && bnng.equals("NNNN")) ||
                                            (NUM_HOLES == 5 && bnng.equals("NNNNN"))){

                                            ControladorPresentacio.finalitzarRonda();
                                            end = true;
                                            s.stop();
                                            s.close();
                                            final_defeat_sound_and_button();
                                        }
                                    }
                                    else {
                                        if (num_jugada < 11){
                                            String candidat = ControladorPresentacio.jugar(bnng);
                                            ++num_jugada;
                                            for (int i = 0; i < candidat.length(); ++i){
                                                posicio_color[num_jugada][i] = Character.digit(candidat.charAt(i),10);
                                            }
                                        }
                                        else {
                                            ControladorPresentacio.finalitzarRonda();
                                            end = true;
                                            s.stop();
                                            s.close();
                                            final_win_sound_and_button();
                                        }
                                    }
                                } catch (Exception exep) {/*missatge no has introduit be les bn's*/}
                                
                            }
                        }
                    }
                }
                else{
                    if (verifyButton.contains(e.getX(), e.getY())){
                        if ((ControladorPresentacio.rondesacabades == 4) || (ControladorPresentacio.modo == "practica")){
                            s.stop();
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        timer.stop();
                                        s.stop();
                                        s.close();
                                        removeMouseListener(mL);
                                        removeMouseMotionListener(mML);
                                        Runtime runtime = Runtime.getRuntime();

                                        double p = (double)(runtime.totalMemory() - runtime.freeMemory()) / (double) runtime.totalMemory();
                                        Stage stage = (Stage) pane.getScene().getWindow();
                                        if (p < 0.85) {
                                            Parent root = FXMLLoader.load(getClass().getResource("Inicio.fxml"));
                                            Scene scene = new Scene(root, 720.0, 480.0);
                                            stage.setTitle("Mastermind");
                                            stage.setScene(scene);
                                            stage.setWidth(720.0);
                                            stage.setHeight(480.0);
                                            if (stage.isFullScreen()) stage.setFullScreen(false);
                                            if (stage.isMaximized()) stage.setMaximized(false);
                                            if (stage.isResizable()) stage.setResizable(false);
                                            stage.show();
                                        }
                                        else{
                                            Runtime.getRuntime().exec("java mastermind.presentacio.ControladorPresentacio");
                                            stage.hide();
                                        }

                                    } catch (Exception b){
                                        System.out.println(b.getMessage());
                                    }
                                }
                            });
                        }
                        else {
                            if (rol) ControladorPresentacio.rol = 0;
                            else ControladorPresentacio.rol = 1;
                            ControladorPresentacio.nueva = true;
                            s.stop();
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        timer.stop();
                                        s.stop();
                                        s.close();
                                        removeMouseListener(mL);
                                        removeMouseMotionListener(mML);
                                        Stage stage = (Stage) pane.getScene().getWindow();
                                        // stage.hide();
                                        Parent root = FXMLLoader.load(getClass().getResource("Taula.fxml"));
                                        Scene scene = new Scene(root, stage.getWidth(), stage.getHeight());
                                        stage.setTitle("Mastermind");
                                        stage.setScene(scene);
                                        if (!stage.isResizable())stage.setResizable(true);
                                        stage.show();


                                    } catch (Exception b){
                                        System.out.println(b.getMessage());
                                    }
                                }
                            });
                        }
                    }
                }

                if (BackButton.contains(e.getX(), e.getY()) && !oneClick){
                    oneClick = true;
                    s.stop();
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                timer.stop();
                                s.stop();
                                s.close();
                                removeMouseListener(mL);
                                removeMouseMotionListener(mML);
                                Runtime runtime = Runtime.getRuntime();

                                double p = (double)(runtime.totalMemory() - runtime.freeMemory()) / (double) runtime.totalMemory();
                                Stage stage = (Stage) pane.getScene().getWindow();
                                if (p < 0.85) {
                                    Parent root = FXMLLoader.load(getClass().getResource("Inicio.fxml"));
                                    Scene scene = new Scene(root, 720.0, 480.0);
                                    stage.setTitle("Mastermind");
                                    stage.setScene(scene);
                                    stage.setWidth(720.0);
                                    stage.setHeight(480.0);
                                    if (stage.isFullScreen()) stage.setFullScreen(false);
                                    if (stage.isMaximized()) stage.setMaximized(false);
                                    if (stage.isResizable()) stage.setResizable(false);
                                    stage.show();
                                }
                                else{
                                    Runtime.getRuntime().exec("java mastermind.presentacio.ControladorPresentacio");
                                    stage.hide();
                                }

                            } catch (Exception b){
                                System.out.println(b.getMessage());
                            }
                        }
                    });
                    
                }

                if (muteButton.contains(e.getX(), e.getY())){
                    if (sound_paused){
                        sound_paused = false;
                        s.play();
                        s.loop();
                    }
                    else{
                        sound_paused = true;
                        s.stop();
                    }
                }

                if (volumeUpButton.contains(e.getX(), e.getY())){
                    volume_up();
                }

                if (volumeDownButton.contains(e.getX(), e.getY())){
                    volume_down();
                }

                repaint();
            }
            public void mouseReleased(MouseEvent e) {
                // System.out.println("relesed");
                if (rol){
                    if (is_pressed && (Math.abs(x_pressed - e.getX()) > gameSize.width/40 || Math.abs(y_pressed - e.getY()) > gameSize.height/40) && colorPressed != -1) {
                        boolean click_somewhere = false;
                        for (int j = 0; j < NUM_HOLES; ++j){
                            if (actual_circles[j].contains(e.getX(), e.getY())){
                                click_somewhere = true;
                                posicio_color[num_jugada][j] = swap(colorPressed, colorPressed = posicio_color[num_jugada][j]);
                            }
                        }
                        if (!click_somewhere) colorPressed = -1;
                    }
                    
                    
                }
                else {
                    if (!solution_set){
                        if (is_pressed && (Math.abs(x_pressed - e.getX()) > gameSize.width/40 || Math.abs(y_pressed - e.getY()) > gameSize.height/40) && colorPressed != -1) {
                            boolean click_somewhere = false;
                            for (int j = 0; j < NUM_HOLES; ++j){
                                if (Solution_circles[j].contains(e.getX(), e.getY())){
                                    click_somewhere = true;
                                    solution_color[j] = swap(colorPressed, colorPressed = solution_color[j]);
                                }
                            }
                            if (!click_somewhere) colorPressed = -1;
                        }
                    }
                    else{
                        if (!end){
                            if (is_pressed && (B_pressed || N_pressed)) {
                                boolean click_somewhere = false;
                                for (int i = 0; i < NUM_HOLES; ++i){
                                    if (actual_bn[i].contains(e.getX(), e.getY())){
                                        int bn_pos_aux = bn_posicio[num_jugada][i];
                                        click_somewhere = true;
                                        if (B_pressed) bn_posicio[num_jugada][i] = 0;
                                        else if (N_pressed) bn_posicio[num_jugada][i] = 1;
                                        else bn_posicio[num_jugada][i] = -1;
                                    }
                                }
                            }
                            else if (is_pressed){
                                for (int i = 0; i < NUM_HOLES; ++i){
                                    if (actual_bn[i].contains(e.getX(), e.getY())) {
                                        if (bn_posicio[num_jugada][i] == 0) {
                                            bn_posicio[num_jugada][i] = 1;
                                        }
                                        else if (bn_posicio[num_jugada][i] == 1){
                                            bn_posicio[num_jugada][i] = -1;
                                        }
                                        else if (bn_posicio[num_jugada][i] == -1){
                                            bn_posicio[num_jugada][i] = 0;
                                        }
                                    }
                                }
                            }
                        }
                        B_pressed = false;
                        N_pressed = false;                   
                    }
                }
                is_clicked = false;
                is_pressed = false;
                repaint();
            }
        }

        private void sound(){
            s = new Sound("mastermind/presentacio/tron.wav");
            gainControl = (FloatControl) s.clip.getControl(FloatControl.Type.MASTER_GAIN);
            gainControl.setValue(-20.0f);
            s.play();
            s.loop();
        }

        private void final_win_sound_and_button(){
            s = new Sound("mastermind/presentacio/victory_song.wav");
            gainControl = (FloatControl) s.clip.getControl(FloatControl.Type.MASTER_GAIN);
            gainControl.setValue(-20.0f);
            if (! sound_paused){
                s.play();
                s.loop();
            }
            VerifyButtonImage = load_image("mastermind/presentacio/verify_win_Button.png");
            repaint();
        }

        private void final_defeat_sound_and_button(){
            s = new Sound("mastermind/presentacio/defeat_song.wav");
            gainControl = (FloatControl) s.clip.getControl(FloatControl.Type.MASTER_GAIN);
            gainControl.setValue(-20.0f);
            if (! sound_paused){
                s.play();
                s.loop();
            }
            VerifyButtonImage = load_image("mastermind/presentacio/verify_defeat_Button.png");
            repaint();
        }

        private void volume_down(){
            if (volume_control > 0.05f){
                volume_control -=0.05f;
                gainControl.setValue((gainControl.getMaximum()-gainControl.getMinimum()) * volume_control + gainControl.getMinimum());
            }
        }

        private void volume_up(){
            if (volume_control < 0.95f){
                volume_control +=0.05f;
                gainControl.setValue((gainControl.getMaximum()-gainControl.getMinimum()) * volume_control + gainControl.getMinimum());
            }            
        }

        private void paint_mute_button_and_up_down(Graphics2D g){
            int radi = Math.min(gameSize.width/15, gameSize.height/15);
            g.setStroke(new BasicStroke(2));
            g.setColor(new Color(30, 33, 36, 255));
            muteButton = new Ellipse2D.Double(gameSize.width*90/100, gameSize.height*90/100, radi, radi);
            volumeUpButton = new Ellipse2D.Double(gameSize.width*84/100, gameSize.height*90/100, radi, radi);
            volumeDownButton = new Ellipse2D.Double(gameSize.width*78/100, gameSize.height*90/100, radi, radi);
            g.fill(muteButton);
            g.fill(volumeUpButton);
            g.fill(volumeDownButton);
            if (!sound_paused) g.drawImage(volume_image, gameSize.width*90/100, gameSize.height*90/100, radi, radi, null);
            else g.drawImage(mute_image, gameSize.width*90/100, gameSize.height*90/100, radi, radi, null);
            g.drawImage(volume_up_image, gameSize.width*84/100, gameSize.height*90/100, radi, radi, null);
            g.drawImage(volume_down_image, gameSize.width*78/100, gameSize.height*90/100, radi, radi, null);
            g.setColor(new Color(0, 0, 0, 255));
        }

        public class Sound {

            public Clip clip;

            public Sound(String fileName) {
                // specify the sound to play
                // (assuming the sound can be played by the audio system)
                // from a wave File
                try {
                    File file = new File(fileName);
                    if (file.exists()) {
                        AudioInputStream sound = AudioSystem.getAudioInputStream(file);
                     // load the sound into memory (a Clip)
                        clip = AudioSystem.getClip(null);
                        clip.open(sound);
                    }
                    else {
                        throw new RuntimeException("Sound: file not found: " + fileName);
                    }
                }
                catch (MalformedURLException e) {
                    e.printStackTrace();
                    throw new RuntimeException("Sound: Malformed URL: " + e);
                }
                catch (UnsupportedAudioFileException e) {
                    e.printStackTrace();
                    throw new RuntimeException("Sound: Unsupported Audio File: " + e);
                }
                catch (IOException e) {
                    e.printStackTrace();
                    throw new RuntimeException("Sound: Input/Output Error: " + e);
                }
                catch (LineUnavailableException e) {
                    e.printStackTrace();
                    throw new RuntimeException("Sound: Line Unavailable Exception Error: " + e);
                }

            // play, stop, loop the sound clip
            }
            public void play(){
                clip.setFramePosition(0);  // Must always rewind!
                clip.start();
            }
            public void loop(){
                clip.loop(Clip.LOOP_CONTINUOUSLY);
            }
            public void stop(){
                clip.stop();
            }

            public void close(){
                clip.close();
            }
        }
    }
}
